/** --- BEGIN COPYRIGHT BLOCK ---
 * This Program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; version 2 of the License.
 * 
 * This Program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this Program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA.
 * 
 * 
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
  --- END COPYRIGHT BLOCK ---  */
/*
 * newentry.c -- CGI program to generate newentry form -- HTTP gateway
 */
#include "dsgw.h"
#include "dbtdsgw.h"

static void emit_file(char* filename, dsgwnewtype* entType);

static void
emit_file (char* filename, dsgwnewtype* entType)
{
    auto FILE* html = dsgw_open_html_file( filename, DSGW_ERROPT_EXIT );
    auto char line[ BIG_LINE ];
    auto int argc;
    auto char **argv;

    while ( dsgw_next_html_line( html, line )) {
	if ( dsgw_parse_line( line, &argc, &argv, 0, dsgw_simple_cond_is_true, NULL )) {
	    if ( dsgw_directive_is( line, DRCT_HEAD )) {
		dsgw_head_begin();
		dsgw_emits ("\n");

	    } else if ( dsgw_directive_is( line, "DS_NEWENTRY_SCRIPT" )) {
		dsgw_emits ("<SCRIPT type=\"text/javascript\">\n"
			    "<!-- Hide from non-JavaScript-capable browsers\n"
			    "var selectedType = -1;\n"
			    "\n"
			    "function typeChange(selectType)\n"
			    "{\n"
			    "    var newType = selectType.selectedIndex;\n"
			    "    if ( newType != selectedType ) {\n"
			    "	selectedType = newType;\n"
			    "	newentryNameFrame.location.href = '"
		            DSGW_URLPREFIX_CGI_HTTP
			    "newentry?context=");
		dsgw_emits(context);
		dsgw_emits( "&file=name&etype=' +\n"
			    "	    escape (selectType.options[newType].value);\n"
			    "    }\n"
			    "}\n"
			    "\n"
			    "var previousLocation = '';\n"
			    "var locationChangedRecently = false;\n"
			    "\n"
			    "function locationChange(nameForm)\n"
			    "{\n"
			    "    var location = nameForm.selectLocation.options[nameForm.selectLocation.selectedIndex].value;\n"
			    "    if ( location != previousLocation ) {\n"
			    "	if ( nameForm.dnsuffix != null ) {\n"
			    "	    if ( location != '' ) {\n"
			    "		nameForm.dnsuffix.blur();\n"
			    "		nameForm.dnsuffix.value = '';\n"
			    "		// In Navigator for Macintosh, the preceding code\n"
			    "		// causes a subsequent focus event in dnsuffix.\n"
			    "		// Prevent dnsuffixFocus from acting on it:\n"
			    "		locationChangedRecently = true;\n"
			    "		setTimeout ('locationChangedRecently = false', 100);\n"
			    "	    } else {\n"
			    "		nameForm.dnsuffix.value = previousLocation;\n"
			    "		nameForm.dnsuffix.focus();\n"
			    "		nameForm.dnsuffix.select();\n"
			    "	    }\n"
			    "	}\n"
			    "	previousLocation = location;\n"
			    "    }\n"
			    "}\n"
			    "\n"
			    "function dnsuffixFocus(nameForm)\n"
			    "{\n"
			    "    var location = nameForm.selectLocation.options[nameForm.selectLocation.selectedIndex].value;\n"
			    "    if ( location != '' && ( ! locationChangedRecently )) {\n"
			    "	if ( nameForm.dnsuffix.value == '' ) {\n"
			    "	    nameForm.dnsuffix.value = location;\n"
			    "	    setTimeout ('newentryNameFrame.document.nameForm.dnsuffix.select()', 75);\n"
			    "	    // This is not done immediately, to avoid interference from mouse-up.\n"
			    "	}\n"
			    "	for ( i = 0; i < nameForm.selectLocation.length; i++ ) {\n"
			    "	    if ( nameForm.selectLocation.options[i].value == '' ) {\n"
			    "		previousLocation = '';\n"
			    "		nameForm.selectLocation.selectedIndex = i;\n"
			    "		break;\n"
			    "	    }\n"
			    "	}\n"
			    "    }\n"
			    "}\n"
			    "\n"
			    "function submitNameForm(nameForm)\n"
			    "{\n"
			    "    if ( nameForm.entryname.value == '' ) {\n");

		dsgw_emit_alert ("newentryNameFrame", "width=400,height=130,resizable",
				 "%s", XP_GetClientStr (DBT_enterNameForNewEntry_));
		dsgw_emits ("	return false;\n"
			    "    } else if ( nameForm.selectLocation.options[nameForm.selectLocation.selectedIndex].value == '' &&\n"
			    "               ( nameForm.dnsuffix == null ||\n"
			    "                 nameForm.dnsuffix.value == '' )) {\n");
		dsgw_emit_alert ("newentryNameFrame", "width=400,height=130,resizable",
				 "%s" ,XP_GetClientStr (DBT_enterLocationForNewEntry_));
		dsgw_emits ("	return false;\n"
			    "    } else {\n"
			    "     open('', 'NewEntryWindow');\n"			    
			    "    }\n"
			    "    return true;\n"
			    "}\n"
			    "\n"
			    "function init()\n"
			    "{\n"
			    "}\n"
			    "\n"
			    "// end hiding -->\n"
			    "</SCRIPT>\n");

	    } else if ( dsgw_directive_is( line, "DS_NEWENTRY_TYPE_BODY" )) {
		dsgw_emitf ("<BODY %s>\n",
			    dsgw_html_body_colors );

	    } else if ( dsgw_directive_is( line, "DS_NEWENTRY_TYPE_FORM" )) {
		dsgw_form_begin ("typeForm", "action=\"javascript:void();\"");
		dsgw_emits ("\n");

	    } else if ( dsgw_directive_is( line, "DS_NEWENTRY_TYPE_SELECT" )) {
		auto dsgwnewtype* ntp;
		dsgw_emits ("<SELECT NAME=\"selectType\" onChange=\"parent.typeChange(this)\">\n");
		for (ntp = gc->gc_newentrytypes; ntp; ntp = ntp->dsnt_next) {
		    dsgw_emitf ("<OPTION VALUE=\"%s\">%s</OPTION>\n",
				ntp->dsnt_template ? ntp->dsnt_template : "",
				ntp->dsnt_fullname ? ntp->dsnt_fullname : "");
		}
		dsgw_emits ("</SELECT>\n" );

	    } else if ( dsgw_directive_is( line, "DS_NEWENTRY_NAME_BODY" )) {
		dsgw_emits ("<BODY onLoad=\"");
		if (entType && entType->dsnt_loccount) {
		    dsgw_emits ("parent.locationChange(document.nameForm);");
		}
		dsgw_emitf ("document.nameForm.entryname.focus()\" %s>\n",
			    dsgw_html_body_colors );
		dsgw_emit_alertForm();

	    } else if ( dsgw_directive_is( line, "DS_NEWENTRY_NAME_FORM" )) {
		dsgw_form_begin ("nameForm", "action=\"" DSGW_URLPREFIX_CGI_HTTP "newentry\""
				 " target=NewEntryWindow"
				 " onSubmit=\"return parent.submitNameForm(this)\"");
		dsgw_emits ("\n");

		if (entType) {
		    if (entType->dsnt_rdnattr) {
			dsgw_emitf ("<INPUT TYPE=\"hidden\" NAME=\"rdntag\" VALUE=\"%s\">\n",
				    entType->dsnt_rdnattr);
		    }
		    if (entType->dsnt_template) {
			dsgw_emitf ("<INPUT TYPE=\"hidden\" NAME=\"entrytype\" VALUE=\"%s\">\n",
				    entType->dsnt_template);
		    }
		}

	    } else if ( dsgw_directive_is( line, "DS_NEWENTRY_LOCATION_BEGIN" )) {
		if ( ! (entType && entType->dsnt_loccount)) {
		    while ( dsgw_next_html_line( html, line )) {
			if ( dsgw_parse_line( line, &argc, &argv, 1, dsgw_simple_cond_is_true, NULL )) {
			    if ( dsgw_directive_is( line, "DS_NEWENTRY_LOCATION_END" )) {
				break;
			    }
			}
		    }
		}

	    } else if ( dsgw_directive_is( line, "DS_NEWENTRY_LOCATION_SELECT" )) {
		dsgw_emits ("<SELECT NAME=\"selectLocation\""
			    " onChange=\"parent.locationChange(this.form)\">\n");
		if (entType) {
		    auto dsgwloc* locarray = gc->gc_newentrylocs;
		    auto const int loccount = gc->gc_newentryloccount;
		    auto int j;
		    for ( j = 0; j < entType->dsnt_loccount; ++j ) {
			auto const int i = entType->dsnt_locations[j];
			if (i < loccount) {
			    dsgw_emits ("<OPTION VALUE=");
			    dsgw_emitf ("\"%s\"", locarray[i].dsloc_dnsuffix); /* XXX should escape '"' in dnsuffix */
			    dsgw_emitf (">%s</OPTION>\n", locarray[i].dsloc_fullname);
			}
		    }
		}

	    } else if ( dsgw_directive_is( line, "DS_NEWENTRY_LOCATION_END" )) {

	    } else if ( dsgw_directive_is( line, "EVALUATE" )) {
		if (entType) {
		    auto int i;
		    for (i = 0; i < argc; ++i) {
			if (!strcmp (argv[i], "entType.fullname")) {
			    if (entType->dsnt_fullname) dsgw_emits (entType->dsnt_fullname);
			} else if (!strcmp (argv[i], "entType.rdnattr")) {
			    if (entType->dsnt_rdnattr) dsgw_emits (entType->dsnt_rdnattr);
			} else if (!strcmp (argv[i], "entType.template")) {
			    if (entType->dsnt_template) dsgw_emits (entType->dsnt_template);
			}
		    }
		}

	    } else if ( dsgw_directive_is( line, "DS_HELP_BUTTON" ) && argc > 0) {
		dsgw_emit_helpbutton (argv[0]);
	    } else {
		dsgw_emits (line);
	    }
	    dsgw_argv_free( argv );
	}
    }
    fclose (html);
}

static char*
compute_newurl()
{
    char* entryType = dsgw_get_cgi_var( "entrytype", DSGW_CGIVAR_REQUIRED );
    char* entryName = dsgw_get_cgi_var( "entryname", DSGW_CGIVAR_REQUIRED );
    char* rdnTag    = dsgw_get_cgi_var( "rdntag",    DSGW_CGIVAR_REQUIRED );
    char* dnSuffix  = dsgw_get_cgi_var( "selectLocation", DSGW_CGIVAR_OPTIONAL );
    char* dn;
    char* edn;
    char* newurl = NULL;
    int escapeName = (strchr (entryName, ',') || strchr (entryName, ';'));

    if (!dnSuffix || !*dnSuffix) {
	dnSuffix = dsgw_get_cgi_var( "dnsuffix",  DSGW_CGIVAR_REQUIRED );
    }
    dn = PR_smprintf("%s=%s%s%s,%s",
		     rdnTag, escapeName ? "\"" : "",
		     entryName, escapeName ? "\"" : "",
		     dnSuffix);
    edn = dsgw_strdup_escaped (dn);
    free(dn);
		     
    newurl = PR_smprintf(DSGW_URLPREFIX_CGI_HTTP "%s?tmplname=%s&amp;context=%s&amp;ADD=1&amp;dn=%s",
			 DSGW_CGINAME_EDIT, entryType, context, edn);
    free(edn);

    return newurl;
}

static int
client_is_authenticated()
{
    auto char* cookie = dsgw_get_auth_cookie();
    auto char* rndstr = NULL;
    auto char* dn = NULL;
    auto int answer = 0;
    if (cookie == NULL) return 0;
    if (dsgw_parse_cookie (cookie, &rndstr, &dn) == 0) {
	if (dn) {
	    answer = 1;
	    free (dn);
	}
	if (rndstr) free (rndstr);
    }
    free (cookie);
    return answer;
}

static dsgwnewtype*
find_entryType (char* query)
{
    auto dsgwnewtype* ntp = gc->gc_newentrytypes;
    if (query && *query) {
	auto char* template = dsgw_ch_strdup (query);
	dsgw_form_unescape (template);
	for ( ; ntp; ntp = ntp->dsnt_next) {
	    if (ntp->dsnt_template && !strcmp (ntp->dsnt_template, template)) {
		break;
	    }
	}
	free (template);
    }
    return ntp;
}

static void
get_request(char *docname, char *etype)
{
    if ( docname == NULL || *docname == '\0' ) {
	emit_file ("newentry.html", NULL);
    } else if ( !strcmp( docname, "type" )) {
	emit_file ("newentryType.html", NULL);
    } else if ( !strcmp( docname, "name" )) {
	/*emit_file ("newentryName.html", find_entryType (getenv ("QUERY_STRING")));*/
	emit_file ("newentryName.html", find_entryType (etype));
    }
}

static void
post_request()
{
    char *newurl = compute_newurl();
    if (client_is_authenticated()) {
	/* Direct the client to GET newurl */
	dsgw_emits( "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01//EN\"\n" );
	dsgw_emits( "    \"http://www.w3.org/TR/html4/frameset.dtd\">\n" );
	dsgw_emits ("<HTML>" );
	dsgw_head_begin();
	dsgw_emitf ("\n<TITLE>%s</TITLE>\n", XP_GetClientStr (DBT_titleNewEntry_));
	dsgw_emits ("</HEAD>\n"
		    "<FRAMESET ROWS=\"*,1\">\n");
	dsgw_emitf ("    <FRAME SRC=\"%s\" NORESIZE>\n", newurl);
	dsgw_emits ("</FRAMESET>\n"
		    "</HTML>\n");
	/* It's tempting to use server redirection, like this:
	   printf ("Location: %s\n\n", newurl);
	   ... but it won't work, because we're handling a POST,
	   and the client should GET newurl.
	*/
    } else {
#ifdef DSGW_DEBUG
	dsgw_log ("dsgw_emit_auth_dest (NULL, %s)\n",
		  newurl ? newurl : "NULL");
#endif
	dsgw_emit_auth_dest (NULL, newurl);
    }
    if (newurl) free (newurl);
}

int
main( argc, argv, env )
    int		argc;
    char	*argv[];
#ifdef DSGW_DEBUG
    char	*env[];
#endif
{
    auto int         reqmethod;
    char            *docname = NULL;
    char            *etype = NULL;

    
    reqmethod = dsgw_init( argc, argv, DSGW_METHOD_POST | DSGW_METHOD_GET);

    docname = dsgw_get_cgi_var("file", DSGW_CGIVAR_OPTIONAL);
    if (docname != NULL && *docname == '/') {
      docname++;
    }
    etype = dsgw_get_cgi_var("etype", DSGW_CGIVAR_OPTIONAL);

    dsgw_send_header();
#ifdef DSGW_DEBUG
    dsgw_logstringarray( "env", env ); 
#endif

    if ( reqmethod == DSGW_METHOD_GET ) {
	get_request(docname, etype);
    } else {
	post_request();
    }
    exit( 0 );
}

/*
  emacs settings
  Local Variables:
  indent-tabs-mode: t
  tab-width: 8
  End:
*/
