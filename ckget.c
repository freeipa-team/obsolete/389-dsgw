/** --- BEGIN COPYRIGHT BLOCK ---
 * This Program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; version 2 of the License.
 * 
 * This Program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this Program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA.
 * 
 * 
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
  --- END COPYRIGHT BLOCK ---  */

#include <stdio.h>
#include "dsgw.h"

#include <ssl.h>
#include <sec.h>

main()
{
    char *p;
    time_t expires;
    char dn[ 512 ];
    char cookie[ 512 ];
    int rc;
    char *pw;
    

    printf( "Retrieve an entry from the cookie database\n" );

    printf( "cookie: " );
    fgets( cookie, sizeof(cookie), stdin );
	if (p = strchr(cookie, '\n')) {
		*p = 0;
	}
    printf( "dn: " );
    fgets( dn, sizeof(dn), stdin );
	if (p = strchr(dn, '\n')) {
		*p = 0;
	}

    rc = dsgw_ckdn2passwd( cookie, dn, &pw );
    if ( rc == 0 ) {
	printf( "Cookie valid, password is <%s>\n", pw );
    } else {
	if ( rc == DSGW_CKDB_KEY_NOT_PRESENT ) {
	    printf( "Cookie/DN pair not found in database\n" );
	} else if ( rc == DSGW_CKDB_EXPIRED ) {
	    printf( "Cookie/DN pair expired\n" );
	} else {
	    printf( "Unknown DB error\n" );
	}
    }
    if ( pw != NULL ) {
	free( pw );
    }
}
