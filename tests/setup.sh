#!/bin/sh

testdir="$1"
hostname=${HOSTNAME:-vmhost.testdomain.com}
sroot=${SROOT:-/home/$USER/dsol}
secdir=${SECDIR:-/home/$USER/save}
port=${PORT:-1300}
secport=`expr $port + 1`
rootdn=${ROOTDN:-"cn=directory manager"}
rootpw=${ROOTPW:-password}
adminpw=admin
needdata=${NEEDDATA:-1}
usessl=${USESSL:-1}
suffix="dc=example,dc=com"
instance=${INST:-ds}
inst=slapd-$instance
resetdata=${RESETDATA:-1}

if [ ! -d $sroot/etc/dirsrv/$inst ] ; then
$sroot/sbin/setup-ds.pl -s -f - <<EOF
[General]
FullMachineName=   $hostname
SuiteSpotUserID=   $USER
[slapd]
ServerPort=   $port
ServerIdentifier=   $instance
Suffix=   $suffix
RootDN=   $rootdn
RootDNPwd=  $rootpw
EOF
sslconf=/tmp/sslconf.$$.ldif
cat > $sslconf <<EOF
dn: cn=encryption,cn=config
changetype: modify
replace: nsSSL3
nsSSL3: on
-
replace: nsSSLClientAuth
nsSSLClientAuth: allowed
-
add: nsSSL3Ciphers
nsSSL3Ciphers: -rsa_null_md5,+rsa_rc4_128_md5,+rsa_rc4_40_md5,+rsa_rc2_40_md5,
 +rsa_des_sha,+rsa_fips_des_sha,+rsa_3des_sha,+rsa_fips_3des_sha,+fortezza,
 +fortezza_rc4_128_sha,+fortezza_null,+tls_rsa_export1024_with_rc4_56_sha,
 +tls_rsa_export1024_with_des_cbc_sha

dn: cn=config
changetype: modify
add: nsslapd-security
nsslapd-security: on
-
replace: nsslapd-ssl-check-hostname
nsslapd-ssl-check-hostname: off
-
replace: nsslapd-secureport
nsslapd-secureport: $secport

dn: cn=RSA,cn=encryption,cn=config
changetype: add
objectclass: top
objectclass: nsEncryptionModule
cn: RSA
nsSSLPersonalitySSL: Server-Cert
nsSSLToken: internal (software)
nsSSLActivation: on

EOF

ldapmodify -x -h $hostname -p $port -D "$rootdn" -w "$rootpw" -c -f $sslconf
rm -f $sslconf

$sroot/lib/dirsrv/slapd-$instance/stop-slapd
cp $secdir/*.db $sroot/etc/dirsrv/slapd-$instance
cp $secdir/pin.txt $sroot/etc/dirsrv/slapd-$instance
$sroot/lib/dirsrv/slapd-$instance/start-slapd

fi

if [ "$needdata" -eq 1 ] ; then
$sroot/lib/dirsrv/$inst/ldif2db.pl -D "$rootdn" -w "$rootpw" -n userRoot -i $sroot/share/dirsrv/data/Example.ldif
sleep 10
fi

rm -rf testtmp
mkdir testtmp

if [ "$usessl" -eq 1 ] ; then
    ldapurl="ldaps://$hostname:$secport"
    # grab CA cert
    certutil -L -d $sroot/etc/dirsrv/$inst -n "CA certificate" -a > testtmp/cacert.asc
    # pin file
    echo "passwordpassword" > testtmp/pwdfile.txt
    # create sec db
    certutil -N -d testtmp -f testtmp/pwdfile.txt
    # import CA cert
    certutil -A -d testtmp -n "CA certificate" -t "CT,," -a -i testtmp/cacert.asc
    port=$secport
else
    ldapurl="ldap://localhost:$port"
fi

DSGW_CONTEXT_DIR=`pwd`/testtmp ; export DSGW_CONTEXT_DIR

sed -e "s#@ldapurl@#$ldapurl#g" \
    -e "s#@suffix@#$suffix#g" \
    -e "s#@dirmgr@#cn=directory manager#g" \
    -e "s#@contextdir@#$DSGW_CONTEXT_DIR#g" \
    -e "s#@htmldir@#$sroot/share/dirsrv/dsgw/html#g" \
    -e "s#@configdir@#$sroot/share/dirsrv/dsgw/config#g" \
    -e "s#^securitypath.*#securitypath	\"testtmp\"#g" \
    config/dsgw.tmpl > testtmp/dsgw.conf

sed -e "s#@ldapurl@#$ldapurl#g" \
    -e "s#@suffix@#$suffix#g" \
    -e "s#@dirmgr@#cn=directory manager#g" \
    -e "s#@contextdir@#$DSGW_CONTEXT_DIR#g" \
    -e "s#@pbhtmldir@#$sroot/share/dirsrv/dsgw/pbhtml#g" \
    -e "s#@pbconfigdir@#$sroot/share/dirsrv/dsgw/pbconfig#g" \
    -e "s#^securitypath.*#securitypath	\"testtmp\"#g" \
    pbconfig/pb.tmpl > testtmp/pb.conf

sed -e "s#@ldapurl@#$ldapurl#g" \
    -e "s#@suffix@#$suffix#g" \
    -e "s#@httpurl@#$SERVER_URL#g" \
    -e "s#^securitypath.*#securitypath	\"testtmp\"#g" \
    orghtml/orgchart.tmpl > testtmp/orgchart.conf

dir=`pwd`

# CGI env. vars
#ADMSERV_CONF_DIR=$dir/testtmp
#ADMSERV_CONF_DIR=$sroot/etc/dirsrv/admin-serv
#export ADMSERV_CONF_DIR
#ADMSERV_LOG_DIR=$dir/testtmp
#export ADMSERV_LOG_DIR
SERVER_URL=http://localhost
export SERVER_URL

pwpfile=/tmp/pwp.$$
cat > $pwpfile <<EOF
User: directory manager
Password: $rootpw

UserDN: cn=directory manager
SIEPWD: $rootpw
EOF

#VGPREFIX="valgrind --tool=memcheck --leak-check=yes --suppressions=$HOME/valgrind.supp --num-callers=40 --suppressions=$testdir/valgrind.supp "
# These are CGI programs - they assume they will run for a very short period of time - they use exit() instead of free() :P
VGPREFIX="valgrind -q --trace-children=yes --tool=memcheck --track-origins=yes --read-var-info=yes --leak-check=no --suppressions=$HOME/valgrind.supp --num-callers=50 --suppressions=$testdir/valgrind.supp "
GDB="gdb -x .gdbinit "
#DEBUGCMD=VALGRIND
#DEBUGCMD="$GDB"

PROGS="lang tutor edit search dosearch domodify auth doauth newentry dnedit csearch"
#PROGS="dosearch"

# use scripts for orgchart perl scripts
#SCRIPTS="org"
#ClientLanguage=en_US ; export ClientLanguage
HTTP_ACCEPT_LANGUAGE=en_US ; export HTTP_ACCEPT_LANGUAGE

#HTTP_COOKIE='nsdsgwauth=rndstr:cn=directory manager' ; export HTTP_COOKIE

runATest() {
    prog="$1"
    shift
    type="$1"
    shift
    mytest="$1" # this is the test filename, not the test name
    shift
    varnum="$1" # which variant
    shift
    variant="$1"

    if [ ! -d results/$prog ] ; then mkdir -p results/$prog ; fi
    basetest=`basename $mytest`$varnum
    echo "Running test $mytest$varnum . . ."
    if [ -n "$varnum" ] ; then
	testtmpfile=/tmp/$prog.$basetest
	cp $mytest $testtmpfile
	echo -n "$variant" >> $testtmpfile
	mytest=$testtmpfile
    else
	testtmpfile=
    fi
    REQUEST_METHOD=$type ; export REQUEST_METHOD
    if [ $type = "GET" ] ; then
	QUERY_STRING="`cat $mytest`" ; export QUERY_STRING
    else
	CONTENT_LENGTH=`wc -c $mytest | cut -f1 -d' '` ; export CONTENT_LENGTH
        CONTENT_TYPE=application/x-www-form-urlencoded ; export CONTENT_TYPE
    fi
    SCRIPT_NAME=/clients/dsgw/bin/$prog ; export SCRIPT_NAME

    exec 4<$pwpfile
    PASSWORD_PIPE=4 ; export PASSWORD_PIPE
    if [ -n "$DEBUGCMD" -a "$DEBUGCMD" = "$GDB" ] ; then
        if [ -f /tmp/$prog.debug.$basetest ] ; then
            echo "break main" > .gdbinit
        else
            echo no match /tmp/$prog.debug.$basetest
            rm -f .gdbinit
        fi
	if [ $type = "POST" ] ; then
	    echo "run < $mytest > results/$prog/$basetest.html" >> .gdbinit
	else
	    echo "run > results/$prog/$basetest.html" >> .gdbinit
	fi
        if [ ! -f /tmp/$prog.debug.$basetest ] ; then
            echo "quit" >> .gdbinit
        fi
	./libtool --mode execute $GDB ./$prog
        if [ -f /tmp/$prog.debug.$basetest ] ; then
            exit 1
        fi
    elif [ "$DEBUGCMD" = "VALGRIND" ] ; then
	VALGRIND="$VGPREFIX --log-file=results/$prog/$basetest.vg"
	if [ $type = "POST" ] ; then
	    ./libtool --mode execute $VALGRIND ./$prog < $mytest > results/$prog/$basetest.html
	else
	    ./libtool --mode execute $VALGRIND ./$prog > results/$prog/$basetest.html
	fi
    else
	if [ $type = "POST" ] ; then
	    ./libtool --mode execute ./$prog < $mytest > results/$prog/$basetest.html
	else
	    ./libtool --mode execute ./$prog > results/$prog/$basetest.html
	fi
    fi

    4<&- # close the pwpfile

    if [ -n "$testtmpfile" -a -f "$testtmpfile" ] ; then
	rm -f "$testtmpfile"
    fi
}

runGetTestsForProg() {
    prog="$1" # test must be in dir of same name
    shift
    getlist=/tmp/gettests.$$
    find $testdir/$prog -name skip -prune -o -name testget.\* -print 2> /dev/null | sort -n > $getlist
    for test in `cat $getlist` ; do
	runATest "$prog" GET "$test"
	ctxnum=1
	for ctx in "" "/" "." "../../../" "somebogusvalue" "pb" "dsgw" ; do
	    if [ -s "$test" ] ; then
		runATest "$prog" GET "$test" .$ctxnum "&context=$ctx&binddn=$rootdn&passwd=$rootpw&password=$rootpw"
	    else
		runATest "$prog" GET "$test" .$ctxnum "context=$ctx&binddn=$rootdn&passwd=$rootpw&password=$rootpw"
	    fi
	    ctxnum=`expr $ctxnum + 1`
	done
    done
    rm -f $getlist
}

runPostTestsForProg() {
    prog="$1" # test must be in dir of same name
    shift
    postlist=/tmp/posttests.$$
    find $testdir/$prog -name skip -prune -o -name testpost.\* -print 2> /dev/null | sort -n > $postlist
    for test in `cat $postlist` ; do
	runATest "$prog" POST "$test"
	ctxnum=1
	for ctx in "" "/" "." "../../../" "somebogusvalue" "pb" "dsgw" ; do
	    runATest "$prog" POST "$test" .$ctxnum "&context=$ctx&binddn=$rootdn&passwd=$rootpw&password=$rootpw"
	    ctxnum=`expr $ctxnum + 1`
	done
    done
    rm -f $postlist
}

# each prog has a subdir containing the GET/POST args and any other test data
for prog in $PROGS ; do
    if [ "$resetdata" -eq 1 ] ; then
        $sroot/lib/dirsrv/$inst/ldif2db.pl -D "$rootdn" -w "$rootpw" -n userRoot -i $sroot/share/dirsrv/data/Example.ldif
        sleep 10
    fi
    runGetTestsForProg "$prog"
    if [ "$resetdata" -eq 1 ] ; then
        $sroot/lib/dirsrv/$inst/ldif2db.pl -D "$rootdn" -w "$rootpw" -n userRoot -i $sroot/share/dirsrv/data/Example.ldif
        sleep 10
    fi
    runPostTestsForProg "$prog"
done

for prog in $SCRIPTS ; do
    getlist=/tmp/gettests.$$
    find $testdir/$prog -name testget.\* -print 2> /dev/null | sort -n > $getlist
    for test in `cat $getlist` ; do
        if [ ! -d results/$prog ] ; then mkdir -p results/$prog ; fi
        basetest=`basename $test`
        echo "Running test $test"
        REQUEST_METHOD=GET ; export REQUEST_METHOD
        QUERY_STRING="`cat $test`" ; export QUERY_STRING
        if [ -n "$DEBUGCMD" -a "$DEBUGCMD" = "$GDB" ] ; then
            perl -d orgchart/$prog
        else
            perl -w -T -t orgbin/$prog > results/$prog/$basetest.html 2> results/$prog/$basetest.errs
        fi
    done
    rm -f $getlist
done

rm -rf $pwpfile .gdbinit
