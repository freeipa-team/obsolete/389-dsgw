#                                               -*- Autoconf -*-
# BEGIN COPYRIGHT BLOCK
# Copyright (C) 2006 Red Hat, Inc.
# All rights reserved.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; version 2
# of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# END COPYRIGHT BLOCK
# Process this file with autoconf to produce a configure script.

AC_PREREQ(2.59)
AC_INIT([dirsrv-gw], [1.1.11], [http://bugzilla.redhat.com/])
AC_CONFIG_SRCDIR([dsgwutil.c])
AM_INIT_AUTOMAKE([1.9 foreign subdir-objects])
AM_MAINTAINER_MODE
AC_CANONICAL_HOST

AC_CONFIG_HEADER([config.h])

# Checks for programs.
AC_PROG_CXX
AC_PROG_CC
AM_PROG_CC_C_O

# disable static libs by default - we only use a couple
AC_DISABLE_STATIC
AC_PROG_LIBTOOL

# Checks for header files.
AC_HEADER_STDC
AC_CHECK_HEADERS([fcntl.h limits.h stdlib.h string.h strings.h sys/file.h sys/param.h sys/socket.h sys/time.h unistd.h])

# Checks for typedefs, structures, and compiler characteristics.
AC_C_CONST
AC_HEADER_STDBOOL
AC_TYPE_SIZE_T
AC_HEADER_TIME
AC_STRUCT_TM

# Checks for library functions.
AC_FUNC_MALLOC
AC_FUNC_MEMCMP
AC_FUNC_SELECT_ARGTYPES
AC_FUNC_STAT
AC_FUNC_STRTOD
AC_FUNC_VPRINTF
AC_CHECK_FUNCS([ftruncate getcwd isascii localtime_r memmove memset select strcasecmp strchr strdup strerror strncasecmp strpbrk strrchr strstr strtoul])

# check for lockf
AC_CHECK_FUNC([lockf], [AC_DEFINE([USE_LOCKF], [1], [use lockf instead of flock])])

PACKAGE_BASE_NAME=`echo $PACKAGE_NAME | sed -e s/-gw//`
AC_SUBST(PACKAGE_BASE_NAME)

# the default prefix - override with --prefix or --with-fhs or --with-fhs-opt
# unfortunately, this must be a literal - it should be $PACKAGE_BASE_NAME
AC_PREFIX_DEFAULT([/opt/dirsrv])

brand=389
capbrand=389
vendor="389 Project"

# check for --with-instconfigdir
AC_MSG_CHECKING(for --with-instconfigdir)
AC_ARG_WITH(instconfigdir,
        AS_HELP_STRING([--with-instconfigdir=/path],
                       [Base directory for configuration directories (default $sysconfdir/$PACKAGE_BASE_NAME)]),
[
  if test $withval = yes ; then
     AC_ERROR([Please specify a full path with --with-instconfigdir])
  fi
  instconfigdir="$withval"
  AC_MSG_RESULT(yes)
],
[
  dnl this value is expanded out in Makefile.am
  instconfigdir='$(sysconfdir)/$(PACKAGE_BASE_NAME)'
  AC_MSG_RESULT(no)
])
AC_SUBST(instconfigdir)

AC_MSG_CHECKING(for --enable-debug)
AC_ARG_ENABLE(debug, AS_HELP_STRING([--enable-debug], [Enable debug features (default: no)]),
[
  AC_MSG_RESULT(yes)
  AC_DEFINE([DSGW_DEBUG], [1], [Enable extra DSGW debugging code])
],
[
  AC_MSG_RESULT(no)
])

# Used for legacy style packaging where we bundle all of the dependencies.
AC_MSG_CHECKING(for --enable-bundle)
AC_ARG_ENABLE(bundle, AS_HELP_STRING([--enable-bundle], [Enable bundled dependencies (default: no)]),
[
  AC_MSG_RESULT(yes)
  bundle="1";
],
[
  AC_MSG_RESULT(no)
  bundle="";
])
AM_CONDITIONAL(BUNDLE,test "$bundle" = "1")
AM_CONDITIONAL(DEBUG,test "$enable_debug" = "yes")

# libtool automatically adds --rpath $libdir to each executable, and
# there is apparently no standard way to disable this.  Also, you cannot
# override rpath with LD_LIBRARY_PATH, so this causes problems if you have
# and old version of nss/nspr installed in the system $libdir, but you
# want to use a different one.  So we're disabling this rpath thing by
# default and adding a --enable-rpath flag if you really, really want
# to do this.
AC_MSG_CHECKING(for --enable-rpath)
AC_ARG_ENABLE(rpath, AS_HELP_STRING([--enable-rpath], [Allow libtool to add an rpath to $libdir (default: no)]))

m4_include(m4/fhs.m4)

CXXLINK_REQUIRED=0
# on most platforms, we will just use perl from PATH
# On some platforms, we cannot.  Why not just use any old
# perl?  Because of perldap.  We use a perldap that is
# compiled to either 32bit or 64bit, so we must use a native
# perl binary compiled with the same bitsize.  On Solaris
# and HP-UX, /usr/bin/perl is 32 bit, so we cannot use
# those with our 64 bit compiled product.
perlexec='/usr/bin/env perl'
case $host in
  *-*-linux*)
    AC_DEFINE([XP_UNIX], [1], [UNIX])
    AC_DEFINE([Linux], [1], [Linux])
    AC_DEFINE([LINUX], [1], [Linux])
    platform="linux"
	;;
  ia64-hp-hpux*)
    AC_DEFINE([XP_UNIX], [1], [UNIX])
    AC_DEFINE([hpux], [1], [HP-UX])
    AC_DEFINE([HPUX], [1], [HP-UX])
    AC_DEFINE([HPUX11], [1], [HP-UX 11])
    AC_DEFINE([HPUX11_23], [1], [HP-UX 11.23])
    AC_DEFINE([CPU_ia64], [], [cpu type ia64])
    AC_DEFINE([OS_hpux], [1], [OS HP-UX])
    AC_DEFINE([_POSIX_C_SOURCE], [199506L], [POSIX revision])
    AC_DEFINE([_HPUX_SOURCE], [1], [Source namespace])
    CXXLINK_REQUIRED=1
    # assume 64 bit
    perlexec='/opt/perl_64/bin/perl'
    platform="hpux"
	;;
  hppa*-hp-hpux*)
    AC_DEFINE([XP_UNIX], [1], [UNIX])
    AC_DEFINE([hpux], [1], [HP-UX])
    AC_DEFINE([HPUX], [1], [HP-UX])
    AC_DEFINE([HPUX11], [1], [HP-UX 11])
    AC_DEFINE([HPUX11_11], [1], [HP-UX 11.11])
    AC_DEFINE([CPU_hppa], [], [cpu type pa-risc])
    AC_DEFINE([OS_hpux], [1], [OS HP-UX])
    AC_DEFINE([_POSIX_C_SOURCE], [199506L], [POSIX revision])
    AC_DEFINE([_HPUX_SOURCE], [1], [Source namespace])
    CXXLINK_REQUIRED=1
	# assume 64 bit
    perlexec='/opt/perl_64/bin/perl'
    platform="hpux"
	;;
  sparc-sun-solaris*)
    AC_DEFINE([XP_UNIX], [1], [UNIX])
    AC_DEFINE([SVR4], [1], [SVR4])
    AC_DEFINE([__svr4], [1], [SVR4])
    AC_DEFINE([__svr4__], [1], [SVR4])
    AC_DEFINE([_SVID_GETTOD], [1], [SVID_GETTOD])
    AC_DEFINE([CPU_sparc], [], [cpu type sparc])
    AC_DEFINE([OS_solaris], [1], [OS SOLARIS])
    AC_DEFINE([sunos5], [1], [SunOS5])
    AC_DEFINE([OSVERSION], [509], [OS version])
    AC_DEFINE([_REENTRANT], [1], [_REENTRANT])
dnl socket nsl and dl are required to link several programs
    LIBSOCKET=-lsocket
    AC_SUBST([LIBSOCKET], [$LIBSOCKET])
    LIBNSL=-lnsl
    AC_SUBST([LIBNSL], [$LIBNSL])
dnl Cstd and Crun are required to link any C++ related code (ICU)
    LIBCSTD=-lCstd
    AC_SUBST([LIBCSTD], [$LIBCSTD])
    LIBCRUN=-lCrun
    AC_SUBST([LIBCRUN], [$LIBCRUN])
    CXXLINK_REQUIRED=1
    # assume 64 bit
    perlexec='/opt/perl5x/bin/perl'
    platform="solaris"
	;;
  *)
esac
AC_SUBST(perlexec)

AM_CONDITIONAL([CXXLINK_REQUIRED], test "$CXXLINK_REQUIRED" = 1)
AM_CONDITIONAL([HPUX],test "$platform" = "hpux")
AM_CONDITIONAL([SOLARIS],test "$platform" = "solaris")

# installation paths - by default, configure will just
# use /usr as the prefix for everything, which means
# /usr/etc and /usr/var.  FHS sez to use /etc and /var.
# The with-fhs-opt option will use the
# prefix, but it's sysconfdir and localstatedir will be
# /etc/opt, and /var/opt.
if test "$with_fhs_opt" = "yes"; then
  # Override sysconfdir and localstatedir if FHS optional
  # package was requested.
  sysconfdir='/etc/opt'
  localstatedir='/var/opt'
elif test "$with_fhs" = "yes"; then
  ac_default_prefix=/usr
  prefix=$ac_default_prefix
  exec_prefix=$prefix
  dnl as opposed to the default /usr/etc
  sysconfdir='/etc'
  dnl as opposed to the default /usr/var
  localstatedir='/var'
fi

# default is to install with admin server
with_adminserver=yes
AC_ARG_WITH(adminserver, AS_HELP_STRING([--with-adminserver], [Install DSGW with Admin Server - default is yes]))

if test "$with_adminserver" != yes ; then
  # relative to datadir
  htmldir=/$PACKAGE_NAME/html
  pbhtmldir=/$PACKAGE_NAME/pbhtml
  orghtmldir=/$PACKAGE_NAME/orghtml
  configdir=/$PACKAGE_NAME/config
  pbconfigdir=/$PACKAGE_NAME/pbconfig
  # root directory for all manuals
  manualdir=/$PACKAGE_NAME/manual
  # the actual manual files are installed here under the lang directory
  manualsubdir=
  propertydir=/$PACKAGE_NAME/properties
  # relative to libdir
  cgibindir=/$PACKAGE_NAME/dsgw-cgi-bin
  perldir=/$PACKAGE_NAME/perl
  # relative to $localstatedir
  cookiedir=/run/$PACKAGE_BASE_NAME/dsgw/cookies
elif test "$with_fhs_opt" = "yes"; then
  # relative to datadir
  htmldir=/dsgw/html
  pbhtmldir=/dsgw/pbhtml
  orghtmldir=/dsgw/orghtml
  configdir=/dsgw/config
  pbconfigdir=/dsgw/pbconfig
  admservdata=/data
  admservhtml=/html
  # root directory for all manuals
  manualdir=/manual
  # the actual manual files are installed here under the lang directory
  manualsubdir=dsgw
  propertydir=/properties/dsgw
  # relative to libdir
  perldir=/perl
  # same as server's cgibindir
  cgibindir=/dsgw-cgi-bin
  # relative to $localstatedir
  cookiedir=/$PACKAGE_BASE_NAME/dsgw/run/cookies
else
  # relative to datadir
  htmldir=/$PACKAGE_BASE_NAME/dsgw/html
  pbhtmldir=/$PACKAGE_BASE_NAME/dsgw/pbhtml
  orghtmldir=/$PACKAGE_BASE_NAME/dsgw/orghtml
  configdir=/$PACKAGE_BASE_NAME/dsgw/config
  pbconfigdir=/$PACKAGE_BASE_NAME/dsgw/pbconfig
  admservdata=/$PACKAGE_BASE_NAME/data
  admservhtml=/$PACKAGE_BASE_NAME/html
  # root directory for all manuals
  manualdir=/$PACKAGE_BASE_NAME/manual
  # the actual manual files are installed here under the lang directory
  manualsubdir=dsgw
  propertydir=/$PACKAGE_BASE_NAME/properties/dsgw
  # relative to libdir
  perldir=/$PACKAGE_BASE_NAME/perl
  # CGI program directory
  cgibindir=/$PACKAGE_BASE_NAME/dsgw-cgi-bin
  # relative to $localstatedir
  cookiedir=/run/$PACKAGE_BASE_NAME/dsgw/cookies
fi

# relative to instconfigdir
contextdir=/dsgw
securitydir=/dsgw
# URIs
cgiuri=/dsgwcmd
dsgwuri=/dsgw
orguri=/org
pburi=/pb

# Check for library dependencies
m4_include(m4/nspr.m4)
m4_include(m4/nss.m4)
m4_include(m4/sasl.m4)
m4_include(m4/openldap.m4)
m4_include(m4/mozldap.m4)
m4_include(m4/icu.m4)
m4_include(m4/adminutil.m4)

AM_CONDITIONAL(OPENLDAP,test "$with_openldap" = "yes")

# write out paths for binary components
AC_SUBST(PACKAGE_NAME)
AC_SUBST(nspr_inc)
AC_SUBST(nspr_lib)
AC_SUBST(nspr_libdir)
AC_SUBST(nss_inc)
AC_SUBST(nss_lib)
AC_SUBST(nss_libdir)
AC_SUBST(sasl_inc)
AC_SUBST(sasl_lib)
AC_SUBST(sasl_libdir)
AC_SUBST(ldapsdk_inc)
AC_SUBST(ldapsdk_lib)
AC_SUBST(ldapsdk_libdir)
AC_SUBST(openldap_inc)
AC_SUBST(openldap_lib)
AC_SUBST(openldap_libdir)
AC_SUBST(ol_libver)
AC_SUBST(adminutil_inc)
AC_SUBST(adminutil_lib)
AC_SUBST(adminutil_libdir)
AC_SUBST(adminutil_ver)
AC_SUBST(icu_lib)
AC_SUBST(icu_libdir)
AC_SUBST(icu_inc)
AC_SUBST(icu_bin)

# write out paths for data/config files
AC_SUBST(cgibindir)
AC_SUBST(cgiuri)
AC_SUBST(dsgwuri)
AC_SUBST(orguri)
AC_SUBST(pburi)
AC_SUBST(propertydir)
AC_SUBST(htmldir)
AC_SUBST(pbhtmldir)
AC_SUBST(orghtmldir)
AC_SUBST(configdir)
AC_SUBST(pbconfigdir)
AC_SUBST(contextdir)
AC_SUBST(securitydir)
AC_SUBST(cookiedir)
AC_SUBST(perldir)
AC_SUBST(manualdir)
AC_SUBST(manualsubdir)
AC_SUBST(admservdata)
AC_SUBST(admservhtml)

# WINNT should be true if building on Windows system not using
# cygnus, mingw, or the like and using cmd.exe as the shell
AM_CONDITIONAL([WINNT], false)

AC_CONFIG_FILES([Makefile])

AC_OUTPUT

# libtool on fedora/rhel contains some gcc-isms which cause problems
# if not using gcc (e.g. Forte on Solaris, aCC on HP-UX)
# we remove them here
if test "$GCC" != yes ; then
   AC_MSG_NOTICE([Not using gcc - fixing libtool to remove gcc-isms . . .])
   cp -p libtool libtool.orig
   cp -p libtool libtool.tmp
   # dnl note the special chars @<:@ and @:>@ - since m4 treats [ and ] specially,
   # we have to use the quadrigraph @<:@ for [ and @:>@ for ] - and you thought
   # perl produced write-only code . . .
   sed -e '/^gcc_dir/ d' \
       -e '/^gcc_ver/ d' \
       -e 's/^predep_objects=.*echo \("@<:@^"@:>@*"\).*$/predep_objects=\1/' \
       -e 's/^postdep_objects=.*echo \("@<:@^"@:>@*"\).*$/postdep_objects=\1/' \
       -e 's/^compiler_lib_search_path=.*echo \("@<:@^"@:>@*"\).*$/compiler_lib_search_path=\1/' \
       -e 's/^sys_lib_search_path_spec=.*echo \("@<:@^"@:>@*"\).*$/sys_lib_search_path_spec=\1/' \
       libtool > libtool.tmp
   cp -p libtool.tmp libtool
   rm -f libtool.tmp
fi

if test "$enable_rpath" != "yes" ; then
   AC_MSG_NOTICE([Fixing libtool to remove automatic rpath to $libdir . . .])
   cp -p libtool libtool.orig
   cp -p libtool libtool.tmp
   sed -e '/^runpath_var/ d' \
       -e '/^hardcode_libdir_flag_spec/ d' \
       libtool > libtool.tmp
   cp -p libtool.tmp libtool
   rm -f libtool.tmp
fi

