<!-- BEGIN COPYRIGHT BLOCK
 This Program is free software; you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation; version 2 of the License.

 This Program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along with
 this Program; if not, write to the Free Software Foundation, Inc., 59 Temple
 Place, Suite 330, Boston, MA 02111-1307 USA.

 
 Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 Copyright (C) 2005 Red Hat, Inc.
 All rights reserved.
  END COPYRIGHT BLOCK -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
    "http://www.w3.org/TR/html4/strict.dtd">
<html>

<head>
<title>Directory Authentication</title>
</head>

<body>

<h1><a name="authenticating"></a>Authentication</h1>

<p>Authentication is the
process of identifying yourself to the Directory Server. The
authentication process enables the Directory Server
to determine what operations you are allowed to perform on the
directory. Note, however, that authentication is not always
necessary; your directory administrator can configure the system
so that permission is not required for some procedures.</p>

<p>By default, access to the directory is denied to all users
with the exception of the directory administrator. However, most sites
allow anonymous search access and self update access.  The
directory administrator defines the permissions that
grant or remove access to the directory. Because permissions are
determined on a site by site basis, you need to check with your
directory administrator to find out what kind of access you have
to the directory and which operations require authentication, if any.</p>

<p>This chapter contains the following sections:</p>

<ul>
    <li><a href="/clients/dsgw/bin/lang?<!-- GCONTEXT -->&file=.MANUAL/auth.htm#ustand">Understanding directory access</a></li>
    <li><a href="/clients/dsgw/bin/lang?<!-- GCONTEXT -->&file=.MANUAL/auth.htm#userauth">Authenticating to the directory</a></li>
    <li><a href="/clients/dsgw/bin/lang?<!-- GCONTEXT -->&file=.MANUAL/auth.htm#logout">Logging out of the directory</a></li>
    <li><a href="/clients/dsgw/bin/lang?<!-- GCONTEXT -->&file=.MANUAL/auth.htm#incorrectauth">Problems caused by improper
        authentication</a></li>
</ul>

<h2><a name="ustand"></a>Understanding Directory Access</h2>

<p>One of the key tasks of the directory administrator
is determining which users need access to the directory and the
types of access required. The directory administrator grants and
denies permission to the directory through the use of the access
control mechanism. Using the access control mechanism, the
directory administrator can allow or deny access:</p>

<ul>
    <li>to any unauthenticated user (this is known as anonymous
        access) </li>
    <li>to all authenticated users</li>
    <li>to specific authenticated users or groups</li>
    <li>from a specific machine or DNS domain</li>
    <li>at a specific time of day or day of the week</li>
    <li>based on authentication method</li>
</ul>

<p>The specific rights the administrator assigns can vary from
user to user. For example, the administrator usually would grant read
and search access to anonymous users and would grant write access
only to a select group of authenticated users and groups, perhaps only
from specific machines.</p>

<p>The following are just some of the things the directory
administrator can do by applying permissions to the directory.
The directory administrator can:</p>

<ul>
    <li>Require you to authenticate before accessing the
        directory in any way. </li>
    <li>Require you to authenticate before accessing certain
        subsections of the directory. </li>
    <li>Require you to authenticate before performing certain
        kinds of actions in the directory, such as adding or
        modifying entries. </li>
    <li>Deny you access to all or parts of the directory,
        or deny you the ability to perform certain kinds of
        functions. </li>
    <li>Allow anonymous access to all or parts of the directory.</li>
    <li>Allow anonymous access for some kinds of operations (such
        as searches), but not others (such as
        modifications). </li>
    <li>Allow or deny access based on the physical machine you
        are currently using. </li>
</ul>

<p>The Directory Server interface has no way of determining if
you are required to authenticate before attempting any directory
access. However, the interface assumes you must authenticate
before modifying the directory tree in any way, and if you are
not currently authenticated, it prompts you for authentication
before you can make any modifications. If you do not
authenticate, you are allowed only to perform the operations
and access the portions of the directory that your directory
administrator has set for anonymous access.</p>

<!--<p>For more information on access control, refer to Chapter 5 of
the <em>Directory Server Administrator's Guide</em>.</p>-->

<h2><a name="userauth"></a>Authenticating to the Directory</h2>

<p>In some situations, the Directory Server interface 
automatically prompts you to authenticate before continuing with
an operation. You can also explicitly choose to authenticate by
clicking the Authentication tab. Either way, the
authentication procedure is as follows:</p>

<ol>
    <li>Click the Authentication tab.</li>
    <li>Enter the name you want to use to identify yourself to
        the Directory Server: 
	  <ul type="disc">
            <li><a name="userauth2"></a>To authenticate as a regular user, enter your
                full name and click Continue. <br>
                Enter your name as it would appear in the
                Directory Server (your common name or full name).
                Do not enter your user ID or login for the local
                operating system. </li>
            <li><a name="managerauth2"></a>To authenticate as the privileged directory user,
                click the "Authenticate as directory manager"
                button.</li>
        </ul>
    </li>
    <li>If the Directory Server interface displays a table of
        matching entries, select the link that corresponds to
        your directory entry. If your name is unique in the
        directory, the system skips this step.</li>
    <li>Enter your password and click Continue.<br>
        Contact your directory manager if you do not know your
        password. <br>
        <a name="authsuccess"></a>After the authentication
        operations complete successfully, the interface displays
        a message indicating the amount of time for which your
        authentication credentials are valid. When this time has
        elapsed, you need to reauthenticate to the directory
        to continue your session. If your password has already
	expired you should either change
	it immediately or contact your system administrator.</li>
    <li>Click "Return to Main" to continue your
        Directory Server interface session. </li>
</ol>

<h2><a name="logout"></a>Logging Out of the Directory</h2>

<p>If you have authenticated to the Directory Server and
want to return to anonymous access, do the following:</p>

<ol>
    <li>Click the Authentication tab.</li>
    <li>Click the "Discard Authentication Credentials (log out)"
        button. </li>
</ol>

<p>You are returned to anonymous access. To change from one type of 
access to another, you must authenticate to the Directory Server again. See <a
href="/clients/dsgw/bin/lang?<!-- GCONTEXT -->&file=.MANUAL/auth.htm#userauth2">Authenticating as a User</a> or <a
href="/clients/dsgw/bin/lang?<!-- GCONTEXT -->&file=.MANUAL/auth.htm#managerauth2">Authenticating as Directory Manager</a> for
more information. </p>

<h2><a name="reauth"></a>Reauthenticating to the Directory</h2>

<p>When you authenticate to the directory, you are given
authentication credentials that are good only for a specific
amount of time. By default, authentication credentials are valid
for 120 minutes. However, this period is configurable by the directory administrator.
If your authentication credentials expire before you have
finished using the Directory Server interface, you must
reauthenticate to the directory before your changes can be saved.
The procedure for reauthenticating to the directory is the same
as the procedure you originally used to <a href="/clients/dsgw/bin/lang?<!-- GCONTEXT -->&file=.MANUAL/auth.htm#userauth">authenticate</a>
to the directory.</p>

<h2><a name="incorrectauth"></a>Problems Caused by Incorrect
Authentication</h2>

<p>When you are not authenticated to the Directory Server, you are
accessing the directory as an anonymous user. The types of
operations you can perform as an anonymous user depend on the
access controls set by your directory administrator. You
may notice strange behavior when you try to perform a directory
operation, such as a search. Although not explicitly stated
by the Directory Server interface, the anomalies you encounter are
often caused by improper authentication. The interface does not provide
this information because doing so could compromise security.</p>

<p>The following table lists symptoms of some common
problems along with the possible causes and the
action you can take to fix the problem.</p>

<table border="2">
    <tr>
        <th width="30%"><b>Symptom</b></th>
        <th><b>Cause</b></th>
        <td width="30%"><b>Action</b></td>
    </tr>
    <tr>
        <td valign="top" width="30%">Search results are empty</td>
        <td valign="top">Either no entries match
        the search string you entered, or you are required to
        authenticate to the directory before performing this type of search
        operation.</td>
        <td valign="top" width="30%">Try a different search
        operation. Or, if you are sure that there are entries
        that match the criteria you entered, <a href="/clients/dsgw/bin/lang?<!-- GCONTEXT -->&file=.MANUAL/auth.htm#userauth">authenticate</a>
        to the directory.</td>
    </tr>
    <tr>
        <td valign="top" width="30%">Search results missing
        entries or missing attribute information from returned
        entries.</td>
        <td valign="top">Either you are not authenticated
        properly or you do not have access to the information.
        The directory administrator can specify that all or parts
        of the directory tree require authentication to access
        entries, or even certain entry attributes. In this situation,
	  the Directory Server does not indicate that the
        information exists and that you do not have 
        privileges to access it. Instead, it simply acts as if
        the information does not exist at all. This behavior is
        driven by the concern that knowing certain information
        exists in the tree, even if you are not allowed to see
        it, can pose a security risk. </td>
        <td valign="top" width="30%">Make sure you are properly <a
        href="/clients/dsgw/bin/lang?<!-- GCONTEXT -->&file=.MANUAL/auth.htm#userauth">authenticated</a>. Then, verify with
        your directory administrator that you have access to the
        directory information you need.</td>
    </tr>
    <tr>
        <td valign="top" width="30%">Operation fails after
        completion</td>
        <td valign="top">The directory is failing the operation
        because of improper authentication. Although, it may seem as if
        the interface's form action is failing the
        operation, the form is only passing the operation to the
        Directory Server, which is then failing the operation.
        The Directory Server interface simply reports the results
        of the operation. This occurs because the LDAP protocol
        does not currently allow the interface to know whether
        authentication is required before trying an operation.
        Using the interface, this situation can only arise
        if your authentication times out while you are creating
        or modifying the directory entry. </td>
        <td valign="top" width="30%">Make sure you are properly <a
        href="/clients/dsgw/bin/lang?<!-- GCONTEXT -->&file=.MANUAL/auth.htm#userauth">authenticated</a> and that your authentication
	  has not timed out. </td>
    </tr>
    <tr>
        <td valign="top" width="30%">A table of entries is
        displayed during the authentication process</td>
        <td valign="top">Either your full name is not unique in
        the directory, or the name you entered does not exist in
        the directory.</td>
        <td valign="top" width="30%">If your entry is displayed
        on the table, select the corresponding link and continue
        with the <a href="/clients/dsgw/bin/lang?<!-- GCONTEXT -->&file=.MANUAL/auth.htm#userauth">authentication</a> process.<p>If
        your entry is not displayed on the table, click Cancel
        and then try <a href="/clients/dsgw/bin/lang?<!-- GCONTEXT -->&file=.MANUAL/auth.htm#userauth">authenticating</a>
        again. Be sure to use your full name and not your user
        ID.</p>
        </td>
    </tr>
    <tr>
        <td valign="top" width="30%">Username is correct, but
        authentication fails anyway</td>
        <td valign="top">Your password is incorrect. <p>If you
        enter a valid username but an incorrect password, and the
        username you supplied represents an NT person entry, the
        Directory Server attempts to authenticate you to the
        Windows network. </p>
        <p>If that is not successful or the user name you
        supplied does not represent an NT person entry, you are
        given the choice to retry, close the window, or seek
        help.</p>
        </td>
        <td valign="top" width="30%">Click Retry
        and then reenter your password.</td>
    </tr>
</table>

<p> </p>
</body>
</html>
