
Developers:
* Building - standard autotools - Do not edit configure or Makefile or Makefile.in -
    Instead, change Makefile.am or configure.ac, then run autoreconf -fvi - do not
    attempt to run aclocal or automake or autoconf - only use autoreconf -fvi - this
    also applies to the files in m4/, that is, if you modify them, you must run
    autoreconf -fvi

* configure - DSGW requires NSPR, NSS, Mozilla LDAP C SDK, ICU, and AdminUtil - these
    can be specified to configure (e.g.--with-nspr=/path), or configure will attempt
    to find these components on the OS using pkg-config or icu-config
    You can also specify --with-admserv=/path/to/admserv to make a DSGW package that
    is compatible with the Admin Server (e.g. installs in clients/dsgw).

* Build process
0. Properties are in dbtdsgw.h - this file is used to generate the resource bundles
    and is also used in the source code to refer to the property by its key
1. build propmaker
2. use propmaker to parse dbtdsgw.h and generate dsgw.properties
3. use genrb_wrapper.sh to "compile" dsgw.properties into dsgw_root.res
4. build the executable CGI programs
5. For a make install, copy the files to their desired directories - if you just
    want to see how the files with be laid out in the file system, use
    make DESTDIR=built install # create destination directory structure under built/
    OR
    make DESTDIR=/tmp/dsgwbuild # create dest dir under /tmp/dsgwbuild/
    rpmbuild will typically do something like the latter

* Setup
1. The configure/make process creates a shell script called setup-ds-dsgw which is
    installed in the sbindir (e.g. /usr/sbin for FHS).  This
    should be run after installation (e.g. after rpm -i).  This gets the information
    from the admin and directory server installation which it looks for under
    /etc/dirsrv.  After running setup, you can edit the files in /etc/dirsrv/dsgw
    if needed (but you will usually not need to).

2. The old Admin Server had the DSGW (Directory Server Express and
    Directory Server Gateway) hard coded in start-console.html - we now replace
    this during setup - the file has this information commented out - during
    setup, if we find that file, we uncomment out the relevant sections and
    change the URL appropriately (e.g. replace @cgiuri@ with the real one)

3. We need to add some information to the httpd.conf file - to enable cgi-bin
    execution, provide access to our HTML pages, and to set the environment
    variable pointing to the config directory - The config directory will
    usually be set during build/packaging, but can be overridden by SetEnv -
    The setup process will copy the file dsgw-httpd.conf into the Apache
    config directory (e.g. /etc/httpd/conf) and change httpd.conf to Include
    the file
