/** --- BEGIN COPYRIGHT BLOCK ---
 * This Program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; version 2 of the License.
 * 
 * This Program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this Program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA.
 * 
 * 
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
  --- END COPYRIGHT BLOCK ---  */
/*
 * unauth.c -- CGI to discard cookies -- HTTP gateway
 */

#include "dsgw.h"
#include "dbtdsgw.h"

void generate_message( int type );

#define	CKEXP_SUCCESS	1
#define	CKEXP_FAILURE	2

int main( int argc, char **argv )
{
    char	*expck;
    char	*authck;
    
    (void)dsgw_init( argc, argv,  DSGW_METHOD_GET );

    authck = dsgw_get_auth_cookie();
    if ( authck == NULL ) {
	/* No cookie.  Generate an informational message. */
	generate_message( CKEXP_SUCCESS );
	free( authck );
	exit( 0 );
    }
    
    /* Remove the cookie from the cookie database */
    (void)dsgw_delcookie( authck );

    /* Generate a cookie header with the cookie set to [unauthenticated] */
    expck = dsgw_ch_malloc( strlen( DSGW_CKHDR ) + strlen( DSGW_AUTHCKNAME ) +
	    strlen( DSGW_UNAUTHSTR ) + strlen( "=; path=/" ) + 2 );
    sprintf( expck, "%s%s=%s; path=/", DSGW_CKHDR, DSGW_AUTHCKNAME, DSGW_UNAUTHSTR );
    dsgw_add_header( expck );
    generate_message( CKEXP_SUCCESS );
    free( authck );
    free( expck );
    exit( 0 );
}

void
generate_message( int type )
{
    dsgw_send_header();
    dsgw_emits( "<HTML>" );
    dsgw_head_begin();
    dsgw_emits( "\n<TITLE>" );
    if ( type == CKEXP_SUCCESS ) {
	dsgw_emits( "Success" );
    } else if ( type == CKEXP_FAILURE ) {
	dsgw_emits( "Error" );
    }
    dsgw_emits( "</TITLE>\n</HEAD>\n" );
    dsgw_emitf( "<BODY %s>\n", dsgw_html_body_colors );

    dsgw_emitf( "<CENTER>\n"
	"<FONT SIZE=+2>\n"
	"%s"
	"</FONT>\n"
	"</CENTER>\n"
	"<br>\n"
	"%s",
	XP_GetClientStr( DBT_Success_ ),
	XP_GetClientStr( DBT_YouAreNoLongerAuthenticated_ ));

    if ( type != CKEXP_SUCCESS ) {
	/*
	 * Something went wrong, so generate some JavaScript to
	 * discard the cookie.
	 */
	dsgw_emits( "<SCRIPT type=\"text/javascript\">\n" );
	dsgw_emitf( "document.cookie = '%s=%s; path=/';\n", DSGW_AUTHCKNAME,
		DSGW_UNAUTHSTR );
	dsgw_emits( "</SCRIPT>\n" );
    }
    dsgw_form_begin (NULL, NULL);
    dsgw_emits( "\n"
	"<TABLE BORDER=2 WIDTH=100%>\n"
	"<TR>\n"
	"<TD ALIGN=CENTER WIDTH=50%>\n");
    dsgw_emitf(
	"<INPUT TYPE=BUTTON VALUE=\"%s\"", XP_GetClientStr( DBT_GoBack_ ));
    dsgw_emits(
	" onClick=\"window.location.href=");
    dsgw_quote_emitf(QUOTATION_JAVASCRIPT, "auth?context=%s", context);
    dsgw_emits(";\"></TD>\n"
	"<TD ALIGN=CENTER WIDTH=50%>\n" );
    dsgw_emit_helpbutton( "UNAUTH" );
    dsgw_emits( "</TABLE></FORM>\n"
	"</BODY></HTML>\n" );
}

