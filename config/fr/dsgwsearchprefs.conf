# BEGIN COPYRIGHT BLOCK
# This Program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; version 2 of the License.
# 
# This Program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with
# this Program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA.
# 
# 
# Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
# Copyright (C) 2005 Red Hat, Inc.
# All rights reserved.
# END COPYRIGHT BLOCK
# 
# dsgwsearchprefs.conf - directory server gateway search object definitions


# the current version of this file format is 1
Version 1


# Name for this search object
People
# options (the only one supported right now is "internal" which means that
#   this search object should not be presented directly to the user)
#   use "" for none
""
# Label to place before text box user types in
"Rechercher :"
# Filter prefix to append to all searches
"(&(objectClass=person)"
# Tag to use for "Fewer Choices" searches - from ldapfilter.conf file
"dsgw-people"
# If a search results in > 1 match, retrieve this attribute to help
# user disambiguate the entries...
not-used-by-dsgw
# ...and label it with this string:
not-used-by-dsgw
# Search scope to use when searching
subtree
# Follows a list of "More Choices" search options.  Format is:
# Label, attribute, select-bitmap, extra attr display name, extra attr ldap name
# If last two are null, "Fewer Choices" name/attributes used.
# Label should begin with the article ("the" in English) for
# languages that require agreement between article and noun
# (e.g genders in Spanish or French).
"nom complet"                     		cn                 111111  ""  ""
"nom"                     			sn                 111111  ""  ""
"numéro de téléphone"     			"telephoneNumber"  111011  ""  ""
"adresse électronique"                	"mail"             111111  ""  ""
"id utilisateur"                      	"uid"              111111  ""  ""
"titre"                         		title              111111 "" ""
END
# Match types
"est"               		"(%a=%v))"
"n'est pas"               	"(!(%a=%v)))"
"ressemble à" 		  	  "(%a~=%v))"
"commence par"                  "(%a=%v*))"
"se termine par"                "(%a=*%v))"
"contient"                      "(%a=*%v*))"
END


"NT-People"
""
"Rechercher :"
"(&(objectClass=ntuser)"
"dsgw-ntpeople"
not-used-by-dsgw
not-used-by-dsgw
subtree
"nom complet"                     		cn                 111111  ""  ""
"nom"                     			sn                 111111  ""  ""
"numéro de téléphone"     			"telephoneNumber"  111011  ""  ""
"adresse électronique"           	"mail"             111111  ""  ""
"id utilisateur"                       	"uid"              111111  ""  ""
"titre"                         		title              111111 "" ""
"nom d'utilisateur NT"                   	"ntuserdomainid"   110000 "" ""
"domaine NT"                     		"ntuserdomainid"   101000 "" ""
"serveur de connexions NT"               	"ntuserlogonserver" 111111 "" ""
END
"est"               		"(%a=%v))"
"n'est pas"               	"(!(%a=%v)))"
"ressemble à"	 		  "(%a~=%v))"
"commence par"                  "(%a=%v*))"
"se termine par"            	  "(%a=*%v))"
"contient"                      "(%a=*%v*))"
END


Groups
""
"Rechercher :"
"(&(|(objectClass=rfc822MailGroup)(objectClass=groupOfNames)(objectClass=groupOfUniqueNames)(objectClass=groupOfCertificates))"
"dsgw-groups"
not-used-by-dsgw
not-used-by-dsgw
subtree
"nom"  	                cn                    111111  ""       ""
"description"                   description  	      111111  ""       ""
"propriétaire (DN)"  		  "owner"               000011  "owner"  "Owner"
"membre (DN)"                   "uniquemember"        000011  ""       ""
END
"est"                     	  "(%a=%v))"
"n'est pas"                     "(!(%a=%v)))"
"ressemble à"			  "(%a~=%v))"
"commence par"                  "(%a=%v*))"
"se termine par"                "(%a=*%v))"
"contient"                      "(%a=*%v*))"
END

NT-Groups
""
"Rechercher :"
"(&(objectClass=ntGroup)"
"dsgw-ntgroups"
not-used-by-dsgw
not-used-by-dsgw
subtree
"nom"  	                cn                    111111  ""       ""
"domaine NT"                     "ntgroupdomainid"   110000 "" ""
"nom du groupe NT"               "ntgroupdomainid"  101000 "" ""
"description"                    description  	      111111  ""       ""
"propriétaire (DN)"       	   "owner"              000011  "owner"  "Owner"
"membre (DN)"                    "uniquemember"       000011  ""       ""
END
"est"                            "(%a=%v))"
"n'est pas"                      "(!(%a=%v)))"
"ressemble à"		   "(%a~=%v))"
"commence par"                   "(%a=%v*))"
"se termine par"                 "(%a=*%v))"
"contient"                       "(%a=*%v*))"
END


Organizations
""
"Rechercher :"
"(&(objectClass=organization)"
"dsgw-organizations"
not-used-by-dsgw
not-used-by-dsgw
subtree
"nom"                          	o			111111  ""       ""
"emplacement"                       l			111111  ""       ""
"numéro de téléphone" 		telephoneNumber		111011  ""       ""
"description"                   	description			111011  ""       ""
END
"est"               			"(%a=%v))"
"n'est pas"               		"(!(%a=%v)))"
"ressemble à" 		        "(%a~=%v))"
"commence par"                   	  "(%a=%v*))"
"se termine par"                      "(%a=*%v))"
"contient"                      	  "(%a=*%v*))"
END


"Org-Units"
""
"Rechercher :"
"(&(objectClass=organizationalUnit)"
"dsgw-orgunits"
not-used-by-dsgw
not-used-by-dsgw
subtree
"nom"                          	ou			111111  ""       ""
"emplacement"                      	l			111111  ""       ""
"numéro de téléphone"   		telephoneNumber		111011  ""       ""
"description"                   	description			111111  ""       ""
END
"est"               		      "(%a=%v))"
"n'est pas"               		"(!(%a=%v)))"
"ressemble à" 		        "(%a~=%v))"
"commence par"                     	  "(%a=%v*))"
"se termine par"                      "(%a=*%v))"
"contient"                      	  "(%a=*%v*))"
END

Anything
""
"Rechercher :"
""
"dsgw-anything"
not-used-by-dsgw
not-used-by-dsgw
subtree
"nom commun" 	           	cn                    111111  ""       ""
"description"                 description           111111  ""       ""
END
"est"               		"(%a=%v)"
"n'est pas"               	"(!(%a=%v))"
"ressemble à"        	  "(%a~=%v)"
"commence par"             	  "(%a=%v*)"
"se termine par"            	  "(%a=*%v)"
"contient"                   	  "(%a=*%v*)"
END

Auth
internal
"Authentifier en tant que :"
"(&(objectClass=person)"
"dsgw-people"
not-used-by-dsgw
not-used-by-dsgw
subtree
"nom commun"                   	cn                 111111  ""  ""
"nom de famille"                   	sn                 111111  ""  ""
"numéro de téléphone" 		"telephoneNumber"  111011  ""  ""
"adresse électronique"             "mail"             111111  ""  ""
"id utilisateur"                   	"uid"              111111  ""  ""
"titre"                         	title              111111 "" ""
END
"est"               			"(%a=%v))"
"n'est pas"               		"(!(%a=%v)))"
"ressemble à" 		     	  "(%a~=%v))"
"commence par"                   	  "(%a=%v*))"
"se termine par"                      "(%a=*%v))"
"contient"                      	  "(%a=*%v*))"
END



