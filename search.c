/** --- BEGIN COPYRIGHT BLOCK ---
 * This Program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; version 2 of the License.
 * 
 * This Program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this Program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA.
 * 
 * 
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
  --- END COPYRIGHT BLOCK ---  */
/*
 * search.c -- CGI program to generate smart search form -- HTTP gateway
 */
#include "dsgw.h"
#include "dbtdsgw.h"
#ifdef DSGW_DEBUG
#include <unistd.h>
#endif
static void get_request(char *docname);
static void do_searchtype_popup( struct ldap_searchobj *sop );


int main( argc, argv, env )
    int		argc;
    char	*argv[];
#ifdef DSGW_DEBUG
    char	*env[];
#endif
{
    auto int reqmethod; 
    char *docname = NULL;
    
    reqmethod = dsgw_init( argc, argv, DSGW_METHOD_GET );

    docname = dsgw_get_cgi_var("file", DSGW_CGIVAR_OPTIONAL);
    if (docname && ! dsgw_valid_docname(docname)) {
        dsgw_error( DSGW_ERR_BADFILEPATH, docname, 
                    DSGW_ERROPT_EXIT, 0, NULL );
    }
    dsgw_send_header();

#ifdef DSGW_DEBUG
    dsgw_logstringarray( "env", env ); 
{
    char	buf[ 1024 ];
    getcwd( buf, sizeof(buf));
    dsgw_log( "cwd: \"%s\"\n", buf );
}
#endif

    if ( reqmethod == DSGW_METHOD_GET ) {
	get_request(docname);
    }
    exit( 0 );
}


static void
get_request(char *docname)
{

    auto char* filename = NULL;
    auto struct ldap_searchobj* sop = NULL;

    if (docname != NULL && *docname == '/') {
	docname++;
    }

    if ( docname == NULL || *docname == '\0' ) {
	filename = "search.html";
    } else if ( !strcmp( docname, "string" )) {
	filename = "searchString.html";
	dsgw_init_searchprefs( &sop );
    }
    if (filename) {
	auto FILE* html = dsgw_open_html_file( filename, DSGW_ERROPT_EXIT );
	auto char line[ BIG_LINE ];
	auto int argc;
	auto char **argv;

	while ( dsgw_next_html_line( html, line )) {
	    if ( dsgw_parse_line( line, &argc, &argv, 0, dsgw_simple_cond_is_true, NULL )) {
		if ( dsgw_directive_is( line, "HEAD" )) {
		    dsgw_head_begin();
		    dsgw_emits ("\n");
		} else if ( dsgw_directive_is( line, "DS_SEARCH_SCRIPT" )) {
		    dsgw_emits ("<SCRIPT type=\"text/javascript\">\n"
				"<!-- Hide from non-JavaScript-capable browsers\n"
				"\n"
				"function validate(sform)\n"
				"{\n"
				"    if (sform.searchstring.value == '') {\n");
/* 
 * It would have been nice to detect when the user pressed return without
 * typing anything into the searchstring area, but on Navigator 2.x, the
 * form variable's value seems to get set *after* the onSubmit handler
 * executes, which is unfortunate.
 */
		    dsgw_emit_alert ("searchFrame", NULL, /* "%s<br>(search base %s)", */
				     XP_GetClientStr (DBT_youDidNotSupplyASearchString_),
				     gc->gc_ldapsearchbase);
		    dsgw_emits ("	return false;\n"
				"    }\n"
				"    sform.searchstring.select();\n"
				"    sform.searchstring.focus();\n"
				"    return true;\n"
				"}\n"
				"\n"
				"function init()\n"
				"{}\n"
				"// End hiding -->\n"
				"</SCRIPT>\n");

		} else if ( dsgw_directive_is( line, "DS_SEARCH_BODY" )) {
		    dsgw_emitf ("<BODY onLoad=\""
			    "document.searchForm.searchstring.select();"
			    "document.searchForm.searchstring.focus();\" %s>\n",
			    dsgw_html_body_colors );
		    dsgw_emit_alertForm();

		} else if ( dsgw_directive_is( line, "DS_SEARCH_FORM" )) {
		    dsgw_form_begin ("searchForm", "action=\"%s\" %s %s",
				     dsgw_getvp( DSGW_CGINUM_DOSEARCH ),
				     "onSubmit=\"return top.validate(this)\"",
				     argc > 0 ? argv[0] : "");
		    dsgw_emitf ("\n"
				"<INPUT TYPE=hidden NAME=\"mode\" VALUE=\"smart\">\n"
				"<INPUT TYPE=hidden NAME=\"base\" VALUE=\"%s\">\n",
				gc->gc_ldapsearchbase );
		} else if ( dsgw_directive_is( line, "DS_SEARCH_BASE" )) {
	            dsgw_emits( gc->gc_ldapsearchbase );
		} else if ( dsgw_directive_is( line, "DS_SEARCH_TYPE" )) {
		    do_searchtype_popup( sop );
		} else if ( dsgw_directive_is( line, "DS_HELP_BUTTON" )) {
		    dsgw_emit_helpbutton (argc > 0 ? argv[0] : "");
		} else {
		    dsgw_emits (line);
		}
		dsgw_argv_free( argv );
	    }
	}
	fclose (html);
    }
}


static void
do_searchtype_popup(
struct ldap_searchobj *sop
)
{
    int first = 1;
    struct ldap_searchobj *so;

    dsgw_emits( "<SELECT NAME=\"type\">\n" );
    for ( so = ldap_first_searchobj( sop ); so != NULL;
	  so = ldap_next_searchobj( sop, so ), first = 0) {
	/* Skip any marked "internal-only" */
	if ( LDAP_IS_SEARCHOBJ_OPTION_SET( so, LDAP_SEARCHOBJ_OPT_INTERNAL )) {
	    continue;
	}
	dsgw_emitf( "<OPTION%s value=\"%s\">%s</OPTION>\n",
		   first ? " selected" : "",
		   so->so_objtypeprompt,
		   dsgw_get_translation( so->so_objtypeprompt ));
    }
    dsgw_emits( "</SELECT>\n" );
}

/*
  emacs settings
  Local Variables:
  indent-tabs-mode: t
  tab-width: 8
  End:
*/
