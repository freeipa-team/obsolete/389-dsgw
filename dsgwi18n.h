/** BEGIN COPYRIGHT BLOCK
 * This Program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; version 2 of the License.
 * 
 * This Program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this Program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA.
 * 
 * 
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * END COPYRIGHT BLOCK **/

#ifndef DSGWI18N_H
#define DSGWI18N_H

#include "prtypes.h"

#include "libadminutil/resource.h"

/*
 * Function prototypes for application and libraries
 */


#ifdef __cplusplus
extern "C" 
{
#endif

/* Given the LibraryName, Language and Token, extracts the string corresponding
   to that library and token from the database in the language requested and
   returns a pointer to the string.  Note: Use the macros XP_GetClientStr() and
   XP_GetAdminStr() defined below to simplify source code. */

/*****************/
/* SetLanguage() */
/*****************/
enum
{
	CLIENT_LANGUAGE,
	ADMIN_LANGUAGE,
	DEFAULT_LANGUAGE
};

PR_EXTERN( void )
SetLanguage(int type, char *language);

/* Set language for Client, Admin and Default, XP_GetStringFromDatabase will
   base on the setting to retrieve correct string for specific language */
 
/***********************/
/* GetClientLanguage() */
/***********************/

PR_EXTERN( char* )
GetClientLanguage(void);

/* Returns a pointer to a string with the name of the language requested by
   the current client; intended to be passed to XP_GetStringFromDatabase()
   and used by the front end macro XP_GetClientStr(). */

/**********************/
/* GetAdminLanguage() */
/**********************/

PR_EXTERN( char* )
GetAdminLanguage(void);

/* Returns a pointer to a string with the name of the language requested by
   the administrator; intended to be passed to XP_GetStringFromDatabase()
   and used by the front end macro XP_GetAdminStr(). */

/************************/
/* GetDefaultLanguage() */
/************************/

PR_EXTERN( char* )
GetDefaultLanguage(void);

/* Returns a pointer to a string with the name of the default language
   for the installation from the configuration file. */

/************************/
/* GetFileForLanguage() */
/************************/

PR_EXTERN( int )
GetFileForLanguage(char* filepath,char* language,char* existingFilepath);

/* Looks for a file in the appropriate language.

   Input: filePath,language
   filePath is of the form "/xxx/xxx/$$LANGDIR/xxx/xxx/filename"
            or of the form "/xxx/xxx/xxx/xxx/filename".
   filename may or may not have an extension.
   language is an Accept-Language list; each language-range will be
     tried as a subdirectory name and possibly as a filename modifier.
     "*" is ignored - default always provided if needed.
     "-" is replaced by "_".
   $$LANGDIR is a special string replaced by language. It is optional.
     For the default case, $$LANGDIR/ is replaced by nothing
     (so // is not created).
   
   Returned: existingPath
   existingFilePath is the path of a satisfactory, existing file.
   if no file is found, an empty string "" is returned.
   
   int returned: -1 if no file found (existingFilePath = "")
                  0 if default file is returned
                  1 if language file is returned (any in list) */

PR_EXTERN( char * )
XP_GetClientStr(int);

/* Return a string from the resource database */

PR_EXTERN( void )
XP_InitStringDatabase(const char *path, const char *dbname);

/* Return the most appropriate locale to use based on 
   the HTTP_ACCEPT_LANGUAGE setting - return memory is
   malloced and should be freed after use
*/
char *dsgw_get_locale_from_accept_language();

#ifdef __cplusplus
}
#endif

#endif /* DSGWI18N_H */
