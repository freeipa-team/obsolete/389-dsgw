/** BEGIN COPYRIGHT BLOCK
 * This Program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; version 2 of the License.
 * 
 * This Program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this Program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA.
 * 
 * 
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * END COPYRIGHT BLOCK **/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "libadminutil/resource.h"
#include "dsgwi18n.h"

#include "unicode/utypes.h"
#include "unicode/udat.h"
#include "unicode/ucal.h"
#include "unicode/unum.h"
#include "unicode/ures.h"

static char *database_name;
static Resource *i18nResource;

/*********************************************************************
  strReplace replaces the first instance of from in target with to.
  from can be "": to is inserted at start of target.
  to can be "": from is removed from target.
  if from is not found, 0 is returned; else 1 is returned.
 *********************************************************************/

static int
strReplace(char* target,char* from,char* to)
{
  /* replace /from/to/ in target */
  
  char* pFrom;
  char* pOldTail;
  int   lenTo;
  
  pFrom = strstr(target,from);
  if (pFrom) {
    pOldTail = pFrom+strlen(from);
    lenTo = strlen(to);
    memmove(pFrom+lenTo,pOldTail,strlen(pOldTail)+1);
    memcpy(pFrom,to,lenTo);
    return 1;
  }
  
  return 0;
}

/*********************************************************************
  statFileDir is a wrapper to stat() that strips trailing slashes
  because stat() on Windows seems to return -1 otherwise.
*********************************************************************/

static int
statFileDir(const char *path,  struct stat *info) {
	int ret, pathlen;
	char *newpath = strdup(path);

	if(newpath == NULL)
		return -1;

	for (pathlen = (strlen(newpath) - 1); pathlen >= 0; pathlen--) {
		if (newpath[pathlen] == '/' || newpath[pathlen] == '\\') {
			newpath[pathlen] = '\0';
		} else {
			break;
		}
	}

	ret = stat(newpath, info);

	if (newpath)
		free(newpath);

	return ret;
}

/*********************************************************************
  GetLanguage is reserved for future use. These APIs are not belong
  to this file. It needs to be moved to somewhere which knows what's
  the current language setting.
 *********************************************************************/

static char *emptyString = "";
 
static char *client_language;
static char *admin_language;
static char *default_language;

PR_IMPLEMENT( void )
SetLanguage(int type, char *language)
{
	switch(type) {
	case CLIENT_LANGUAGE:
		if (language)
			client_language = PL_strdup(language);
		break;
	case ADMIN_LANGUAGE:
		if (language)
			admin_language = PL_strdup(language);
		break;
	case DEFAULT_LANGUAGE:
		if (language)
			default_language = PL_strdup(language);
		break;
	}
	return ;
}



PR_IMPLEMENT( char* )
GetClientLanguage(void)
{
  if (client_language)
	return client_language;
  else
	return emptyString;
}
 
PR_IMPLEMENT( char* )
GetAdminLanguage(void)
{
  if (admin_language)
	return admin_language;
  else
	return emptyString;
}

PR_IMPLEMENT( char* )
GetDefaultLanguage(void)
{
  if (default_language)
	return default_language;
  else
	return "en";
}

/*********************************************************************
  GetFileForLanguage looks for a file in the appropriate language.
 *********************************************************************/

PR_IMPLEMENT( int )
GetFileForLanguage(char* filePath,char* language,char* existingFilePath)
{
  /* Input: filePath,language
   * filePath is of the form "/xxx/xxx/$$LANGDIR/xxx/xxx/filename"
   *          or of the form "/xxx/xxx/xxx/xxx/filename".
   * filename may or may not have an extension.
   * language is an Accept-Language list; each language-range will be
   *   tried as a subdirectory name and possibly as a filename modifier.
   *   "*" is ignored - default always provided if needed.
   *   "-" is replaced by "_".
   * $$LANGDIR is a special string replaced by language. It is optional.
   *   For the default case, $$LANGDIR/ is replaced by nothing
   *   (so // is not created).
   *
   * Returned: existingPath
   * existingFilePath is the path of a satisfactory, existing file.
   * if no file is found, an empty string "" is returned.
   *
   * int returned: -1 if no file found (existingFilePath = "")
   *                0 if default file is returned
   *                1 if language file is returned (any in list)
   *
   * Example:
   *    filePath               = "/path/$$LANGDIR/filename.ext"
   *    language               = "language"
   *    GetDefaultLanguage() --> "default"
   *    LANG_DELIMIT           = "_"
   *  
   * 1. Try: "/path/language/filename.ext"
   * 2. Try: "/path/filename_language.ext"
   * 3. Try: "/path/default/filename.ext"
   * 4. Try: "/path/filename_default.ext"
   * 5. Try: "/path/filename.ext"
   *   else: ""
   *
   * Example:
   *    language               = "en-us;q=0.6,ja;q=0.8,en-ca"
   *  
   * 1. Try: "/path/en-ca/filename.ext"
   * 2. Try: "/path/filename_en_ca.ext"
   * 3. Try: "/path/ja/filename.ext"
   * 4. Try: "/path/filename_ja.ext"
   * 5. Try: "/path/en_us/filename.ext"
   * 6. Try: "/path/filename_en_us.ext"
   * 7. Try: "/path/default/filename.ext"
   * 8. Try: "/path/filename_default.ext"
   * 9. Try: "/path/filename.ext"
   *   else: ""
   *
   */

#define LANG_DELIMIT '_'

  int pattern;
  char* pDot;
  char* pSlash;

  /* PRFileInfo info; */
  struct stat info;

  char lang_modifier[MAX_ACCEPT_LENGTH+1];

  ACCEPT_LANGUAGE_LIST acceptLanguageList;
  int numLang;
  int iLang;
  int iCase;


  /* escape in case XP_InitStringDatabase has not been called */
  if (filePath==NULL) {
    *existingFilePath = '\0';
    return -1;
  }

  pattern = (strstr(filePath,"$$LANGDIR/")!=NULL);

  for ( iCase=1 ; iCase>=0 ; iCase-- ) {
    if (iCase==1) {             /* iCase=1 tries requested language */
      numLang = XP_AccLangList(language,acceptLanguageList);
    } else {                    /* iCase=0 tries default language */
      numLang = XP_AccLangList(GetDefaultLanguage(),acceptLanguageList);
    }
    
    for ( iLang=0 ; iLang<numLang ; iLang++ ) {
      
      /* Try: /path/language/filename.ext */
      if (pattern) {
        strcpy(existingFilePath,filePath);
        strReplace(existingFilePath,"$$LANGDIR",acceptLanguageList[iLang]);

        if (statFileDir(existingFilePath,&info)==0) {
          return iCase;
        }

        /*
          if (PR_GetFileInfo(existingFilePath,&info)==PR_SUCCESS) {
          return iCase;
          }
          */
      }
      
      /* Try: /path/filename_language.ext */
      {
        strcpy(existingFilePath,filePath);
        strReplace(existingFilePath,"$$LANGDIR/",emptyString);
        pDot = strrchr(existingFilePath,'.');
        pSlash = strrchr(existingFilePath,'/');
        if (pSlash>=pDot) {
          pDot = strchr(existingFilePath,'\0');
        }
        sprintf(lang_modifier,"%c%s",LANG_DELIMIT,acceptLanguageList[iLang]);
        strReplace(pDot,emptyString,lang_modifier);

        if (statFileDir(existingFilePath,&info)==0) {
          return iCase;
        }

        /*
          if (PR_GetFileInfo(existingFilePath,&info)==PR_SUCCESS) {
          return iCase;
          }
          */
      }
    }
  }
  
  /* Try: /path/filename.ext */
  {
    strcpy(existingFilePath,filePath);
    strReplace(existingFilePath,"$$LANGDIR/",emptyString);

    if (statFileDir(existingFilePath,&info)==0) {
      return 0;
    }

    /*
      if (PR_GetFileInfo(existingFilePath,&info)==PR_SUCCESS) {
      return 0;
      }
      */
  }

  /* Else: */
  *existingFilePath = '\0';
  return -1;
}

/*
  Note: This function returns allocated memory.  Most of the callers in the
  dsgw do not free this memory - they prefer to use exit() for free() - which
  is usually fine for short lived CGI programs - so if you use valgrind you
  will see a lot of memory leakage around this function
*/
PR_IMPLEMENT( char * )
XP_GetClientStr(int key)
{
    int rc = 0;
    char keybuf[256];
    char *lang = GetClientLanguage();
    char *resstring = NULL;

    PR_snprintf(keybuf, sizeof(keybuf), "%s%d", database_name, key);
    resstring = res_getstring(i18nResource, keybuf, lang,
                              NULL, 0, &rc);
    if (rc) {
        fprintf(stderr, "The message keyword id [%d] was not found\n", key);
    }
    return resstring;
}

PR_IMPLEMENT( void )
XP_InitStringDatabase(const char *path, const char *dbname)
{
    database_name = strdup(dbname);
    i18nResource = res_init_resource(path, NULL);
    /* set default languages for string database */
    SetLanguage(CLIENT_LANGUAGE, "");
    SetLanguage(ADMIN_LANGUAGE, "");
    SetLanguage(DEFAULT_LANGUAGE, "");
}

/*
  This function will return the appropriate locale to use
  for ICU functions based on the HTTP_ACCEPT_LANGUAGE
*/
char *
dsgw_get_locale_from_accept_language()
{
    UErrorCode err = U_ZERO_ERROR;
    UEnumeration *available = ures_openAvailableLocales(NULL, &err);
    UAcceptResult outResult;
    char *returnlocale = NULL;
    int32_t needlen = 0;

    if (U_FAILURE(err)) {
        fprintf(stderr, "Error: ures_openAvailableLocales(): %d:%s\n", err, u_errorName(err));
        return NULL;
    }

    needlen = 20;
    returnlocale = (char *)malloc(sizeof(char) * needlen);
    needlen = uloc_acceptLanguageFromHTTP(returnlocale, needlen, &outResult, GetClientLanguage(),
                                          available, &err);

    if(err == U_BUFFER_OVERFLOW_ERROR) {
        err = U_ZERO_ERROR;
        returnlocale = (char *)realloc(returnlocale, sizeof(char) * (needlen + 1));
        needlen = uloc_acceptLanguageFromHTTP(returnlocale, needlen, &outResult, GetClientLanguage(),
                                              available, &err);
    }

    if (U_FAILURE(err)) {
        free(returnlocale);
        returnlocale = NULL;
        fprintf(stderr, "Error: uloc_acceptLanguageFromHTTP(%s): %d:%s\n", GetClientLanguage(), err, u_errorName(err));
        return NULL;
    }

    return returnlocale;
}
