/** --- BEGIN COPYRIGHT BLOCK ---
 * This Program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; version 2 of the License.
 * 
 * This Program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this Program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA.
 * 
 * 
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
  --- END COPYRIGHT BLOCK ---  */
/* 
 * tutor.c - Take a qs, and spit out the appropriate tutorial
 *
 * All blame to Mike McCool
 */

#include <stdio.h>
#include <stdlib.h>
#include "dsgw.h"

#define BASE_MAN_DIRECTORY "manual/"
#define HELP_INDEX_HTML "index.html"

FILE *
_open_html_file( char *filename )
{
    FILE *f;
    char *mypath;
    char *p;

    p = dsgw_file2path( DSGW_MANROOT, DSGW_MANSUBDIR );
    mypath = PR_smprintf("%s/%s", p, filename);

    if (!(f = fopen( mypath, "r" ))) {
	dsgw_error( DSGW_ERR_OPENHTMLFILE, filename, DSGW_ERROPT_EXIT,
		0, NULL );
    }

    free( p );
    PR_smprintf_free( mypath );

    return f;
}



/* Had to copy and paste so wouldn't set referer. */
void _my_return_html_file(char *filename, char *base)  {
    char line[BIG_LINE];
    FILE *html = _open_html_file(filename);

    if(base)  {
        char *tmp;
        char *surl=dsgw_server_url();
        char *sn=dsgw_ch_strdup(getenv("SCRIPT_NAME"));
        tmp=strchr(&(sn[1]), '/');
        *tmp='\0';
        dsgw_emitf("<BASE href=\"%s%s/%s\">\n", surl, sn, base);
    }
    while( fgets(line, sizeof(line), html))  {
	dsgw_emits( line );
    }
}


int
main(
    int		argc,
    char	*argv[]
#ifdef DSGW_DEBUG
    ,char	*env[]
#endif
)
{
    char *param = NULL;
    char *html=NULL;
    char *base=NULL;
    int isgood = 0;

#ifdef DSGW_DEBUG
   dsgw_logstringarray( "env", env ); 
#endif

    dsgw_init( argc, argv, DSGW_METHOD_GET );

    param = dsgw_get_cgi_var("tutor", DSGW_CGIVAR_OPTIONAL);
    if (!param || !*param) {
        dsgw_send_header();
        _my_return_html_file(BASE_MAN_DIRECTORY HELP_INDEX_HTML, NULL);
        exit(0);
    }

    if (param[0] == '!') {
	isgood = dsgw_valid_docname((param+1));
    } else {
	isgood = dsgw_valid_docname(param);
    }

    if (!isgood) {
	dsgw_error( DSGW_ERR_OPENHTMLFILE, param, DSGW_ERROPT_EXIT,
		0, NULL );
	exit( 1 );
    }

    if(param[0]=='!')  {
        param++;
	char *man_path = dsgw_file2path ( DSGW_MANROOT, DSGW_MANSUBDIR );
	char line[BIG_LINE];
	FILE *map=NULL;
	char *man_index=NULL;

	man_index = PR_smprintf("%s/%s", man_path, "index.map" );

	map=fopen(man_index, "r");
	if(!map) 
	    goto ohwell;
	while(fgets(line, sizeof(line), map))  {
	    if(line[0]==';')  
		continue;
	    else if(ldap_utf8isspace(line))
		continue;
	    else  {
		/* parse out the line */
		register char *head=NULL, *tail=NULL;
		int found;

		head=&(line[0]);
		tail=head;
		found=0;
		while(*tail)  {
		    if(ldap_utf8isspace(tail) || *tail=='=')  {
			*tail='\0';
			found=1;
			/* get rid of extra stuff at the end */
			tail++;
			while(1) {
			    if (*tail == 0) {
				++tail; /* This looks wrong. */
				break;
			    }
			    LDAP_UTF8INC(tail);
			    if((!ldap_utf8isspace(tail)) && (*tail!='='))
				break;
			}
			break;
		    }
		    LDAP_UTF8INC(tail);
		}
		if(!found) continue;

		/* script name is in head */
		if(strncasecmp(head, param, strlen(param)))  {
		    continue;
		}
		/* match found.  get the actual file name */
		head=tail;
/* Step on CRs and LFs. */
		while(*tail)  {
		    if((*tail=='\r') || (*tail=='\n') || (*tail==';'))  {
			*tail='\0';
			break;
		    }
		    LDAP_UTF8INC(tail);
		}
		/* assumedly, head should now have the proper HTML file
		 * from the manual inside. redirect the client 'cause
		 * there's no other way to get them to jump to the 
		 * right place. 
		 * Looks like: 
		 * http://host:port/dsgw/bin/lang?context=CONTEXT&file=.MANUAL/FILE.HTM
		 * Where MANUAL is literal
		 */
		html = PL_strdup(gc->gc_urlpfxmain);
		unescape_entities(html);
		dsgw_emitf("Location: %s%s/%s\n\n", 
			   html, DSGW_MANUALSHORTCUT, head);
		free(html);
		fclose(map);
		exit(0);
	    }
	}
	fclose(map);
	free( man_index );

    ohwell:
	if(!html || !html[0])
	    html = PR_smprintf( "%s%s.html", BASE_MAN_DIRECTORY, param);
	free(man_path);
        dsgw_send_header();
        _my_return_html_file(html, base);
    }  else  {
        dsgw_send_header();
        dsgw_emits("<TITLE>Directory Server Gateway Help</TITLE>\n");
        dsgw_emits("\n"); 
        dsgw_emits("<frameset BORDER=\"0\" FRAMEBORDER=\"NO\" rows=\"57,*\" "
		"onLoad=\"top.master=top.opener.top;top.master.helpwin=self;\" "
		"onUnload=\"if (top.master) { top.master.helpwin=0; }\">\n" );
        dsgw_emitf("<frame src=\"%s?file=%s/infonav.html&amp;context=%s\" scrolling=\"no\" "
		   "marginwidth=\"0\" marginheight=\"0\" "
		   "name=\"infobuttons\">\n", dsgw_getvp(DSGW_CGINUM_LANG), DSGW_MANUALSHORTCUT,
		   context);
        dsgw_emitf("<frame src=\"%s?tutor=!%s&amp;context=%s\" "
		   "name=\"infotopic\">\n", dsgw_getvp(DSGW_CGINUM_TUTOR), param, context);
        dsgw_emits("</frameset>\n");
    }
    return 0;
}

/*
  emacs settings
  Local Variables:
  indent-tabs-mode: t
  tab-width: 8
  End:
*/
