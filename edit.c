/** --- BEGIN COPYRIGHT BLOCK ---
 * This Program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; version 2 of the License.
 * 
 * This Program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this Program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA.
 * 
 * 
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
  --- END COPYRIGHT BLOCK ---  */
/*
 * edit.c -- CGI editable entry display -- HTTP gateway
 */

#include "dsgw.h"
#include "dbtdsgw.h"

static void get_request(char *dn, char *tmplname, 
			unsigned long options);


int main( argc, argv, env )
    int		argc;
    char	*argv[];
#ifdef DSGW_DEBUG
    char	*env[];
#endif
{


    char		*dn, *tmplname;
    char *add = NULL;
    unsigned long	options;

    /*
     * If the QUERY_STRING is non-NULL, it looks like this:
     *
     *     template [&CONTEXT=context] [ &INFO=infostring ] [ &ADD ] [ &DN=dn ] \
     *              [&DNATTR=attrname&DNDESC=description]
     *
     * where:
     *   "template" is the name of the edit template to use for display,
     *   "dn" is escaped dn,
     *   "infostring" is a message used to replace DS_LAST_OP_INFO directives
     *   "attrname" is the name of a DN-valued attribute
     *   "dndesc" is the destriptive name of the above DN-valued attribute
     *
     * If "&ADD" is present, we check to make sure the entry
     * does not exist, then we check that the parent entry exists, and then
     * we present an "add entry" form.
     *
     * Note: original form http://host/edit/dn[/...]?template[&...] is
     *       supported for keeping backward compatibility.
     *       But passing DN as PATH_INFO is NOT recommended.
     *       Since PATH_INFO is passed to CGI as is (non-escaped),
     *       the content has a risk to get broken especially when
     *       it contains 8-bit UTF-8 data.  (This is a known problem
     *       on localized Windows machines.)
     *
     * NOTE: The new code uses adminutil to parse the get/post arguments.
     * This requires name=value pairs.  So instead of just ADD, the
     * argument must be ADD=1 (or some other value).  Also, instead of
     * just "template", the argument should be tmplname=template.
     */

    options = DSGW_DISPLAY_OPT_EDITABLE;
    dn = NULL;
#ifndef  __LP64__
#ifdef HPUX
#ifndef __ia64
	/* call the static constructors in libnls */
	_main();
#endif
#endif
#endif

    (void)dsgw_init( argc, argv,  DSGW_METHOD_GET );

    dsgw_last_op_info = dsgw_get_cgi_var("info", DSGW_CGIVAR_OPTIONAL);
    dn = dsgw_get_cgi_var("dn", DSGW_CGIVAR_OPTIONAL);
    dsgw_dnattr = dsgw_get_cgi_var("dnattr", DSGW_CGIVAR_OPTIONAL);
    dsgw_dndesc = dsgw_get_cgi_var("dndesc", DSGW_CGIVAR_OPTIONAL);
    add = dsgw_get_cgi_var("add", DSGW_CGIVAR_OPTIONAL);
    if (add && *add) {
        options |= DSGW_DISPLAY_OPT_ADDING;
    }
    PL_strfree(add);
    tmplname = dsgw_get_cgi_var("tmplname", DSGW_CGIVAR_OPTIONAL);

    dsgw_send_header();

#ifdef DSGW_DEBUG
   dsgw_logstringarray( "env", env ); 
#endif

    get_request(dn, tmplname, options);

    exit( 0 );
}


static void
get_request(char *dn, char *tmplname, unsigned long options)
{
    LDAP		*ld;

    if ( dn == NULL ) { /* not found in QUERY_STRING */
	dsgw_error( DSGW_ERR_MISSINGINPUT, NULL, DSGW_ERROPT_EXIT, 0, NULL );
    }

#ifdef DSGW_DEBUG
    dsgw_log( "get_request: dn: \"%s\", tmplname: \"%s\" "
	      "dnattr: \"%s\", dndesc: \"%s\"\n", dn,
	    ( tmplname == NULL ) ? "(null)" : tmplname, 
	    ( dsgw_dnattr == NULL ) ? "(null)" : dsgw_dnattr, 
	    ( dsgw_dndesc == NULL ) ? "(null)" : dsgw_dndesc );
#endif

    (void)dsgw_init_ldap( &ld, NULL, 0, 0);

    if (( options & DSGW_DISPLAY_OPT_ADDING ) == 0 ) {
	/*
	 * editing an existing entry -- if no DN is provided and we are running
	 * under the admin server, try to get DN from admin. server
	 */
	if ( *dn == '\0' ) {
	    (void)dsgw_get_adm_identity( ld, NULL, &dn, NULL,
		    DSGW_ERROPT_EXIT );
	}

	dsgw_read_entry( ld, dn, NULL, tmplname, NULL, options );

    } else {
	dsgwtmplinfo        *tip;
    	char		    *matched;

	/*
	 * new entry -- check to make sure it doesn't exist
	 */
	if ( dsgw_ldap_entry_exists( ld, dn, &matched, DSGW_ERROPT_EXIT )) {
	    char	**rdns;

	    dsgw_html_begin( XP_GetClientStr(DBT_entryAlreadyExists_), 1 );
	    dsgw_emits( XP_GetClientStr(DBT_anEntryNamed_) );
	    rdns = dsgw_ldap_explode_dn( dn, 1 );
	    dsgw_html_href(
		    dsgw_build_urlprefix(),
		    dn, ( rdns == NULL || rdns[ 0 ] == NULL ) ? dn : rdns[ 0 ],
		    NULL, XP_GetClientStr(DBT_onmouseoverWindowStatusClickHere_) );
	    if ( rdns != NULL ) {
		dsgw_charray_free( rdns );
	    }
	    dsgw_emits( XP_GetClientStr(DBT_alreadyExistsPPleaseChooseAnothe_) );

	    dsgw_form_begin( NULL, NULL );
	    dsgw_emits( "\n<CENTER><TABLE border=2 width=\"100%\"><TR>\n" );
	    dsgw_emits( "<TD WIDTH=\"50%\" ALIGN=\"center\">\n" );
	    dsgw_emitf( "<INPUT TYPE=\"button\" VALUE=\"%s\" "
		"onClick=\"parent.close()\">", XP_GetClientStr(DBT_closeWindow_1) );
	    dsgw_emits( "<TD WIDTH=\"50%\" ALIGN=\"center\">\n" );
	    dsgw_emit_helpbutton( "ENTRYEXISTS" );
	    dsgw_emits( "\n</TABLE></CENTER></FORM>\n" );
	    dsgw_html_end();
	} else if ( !dsgw_is_dnparent( matched, dn ) &&
		!dsgw_dn_cmp( dn, gc->gc_ldapsearchbase )) {
            char *parent = NULL;
	    /*
	     * The parent entry does not exist, and the dn being added is not
	     * the same as the suffix for which the gateway is configured.
	     */
	    dsgw_html_begin( XP_GetClientStr(DBT_parentEntryDoesNotExist_), 1 );
	    dsgw_emitf( XP_GetClientStr(DBT_youCannotAddAnEntryByTheNamePBSB_),
		    dn );
	    parent = dsgw_dn_parent( dn );
	    if ( parent == NULL || strlen( parent ) == 0 ) {
		dsgw_emits( XP_GetClientStr(DBT_itsParentN_) );
	    } else {
		dsgw_emitf( XP_GetClientStr(DBT_anEntryNamedPBSBN_), parent );
		free( parent );
	    }
	    dsgw_form_begin( NULL, NULL );
	    dsgw_emits( "\n<CENTER><TABLE border=2 width=\"100%\"><TR>\n" );
	    dsgw_emits( "<TD WIDTH=\"50%\" ALIGN=\"center\">\n" );
	    dsgw_emitf( "<INPUT TYPE=\"button\" VALUE=\"%s\" "
		"onClick=\"parent.close()\">", XP_GetClientStr(DBT_closeWindow_2) );
	    dsgw_emits( "<TD WIDTH=\"50%\" ALIGN=\"center\">\n" );
	    dsgw_emit_helpbutton( "ADD_NOPARENT" );
	    dsgw_emits( "\n</TABLE></CENTER></FORM>\n" );
	    dsgw_html_end();
	} else {
	    /*
	     * The parent exists, or the user is adding the entry whose DN
	     * is the same as the suffix for which the gateway is configured.
	     * Display the "add entry" form.
	     */

	    if ( tmplname == NULL ) {
#ifdef DSGW_DEBUG
                dsgw_log( "NULL tmplname\n" );
#endif
		dsgw_error( DSGW_ERR_MISSINGINPUT,
			XP_GetClientStr(DBT_missingTemplate_),
			DSGW_ERROPT_EXIT, 0, NULL );
	    }

	    tip = dsgw_display_init( DSGW_TMPLTYPE_DISPLAY, tmplname, options );
     
	    dsgw_display_entry( tip, ld, NULL, NULL, dn );
	    dsgw_display_done( tip, dn );
	}
    }

    ldap_unbind_ext( ld, NULL, NULL );
}

/*
  emacs settings
  Local Variables:
  indent-tabs-mode: t
  tab-width: 8
  End:
*/
