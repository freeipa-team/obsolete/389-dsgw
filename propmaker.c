/** BEGIN COPYRIGHT BLOCK
 * This Program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; version 2 of the License.
 * 
 * This Program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this Program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA.
 * 
 * 
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * END COPYRIGHT BLOCK **/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define RESOURCE_STR

/********************************************/
/* Begin: Application dependent information */
/********************************************/

#include "dbtdsgw.h"
#define GSXXX_H_INCLUDED

/********************************************/
/*  End: Application dependent information  */
/********************************************/

/*******************************************************************************/

/*******************************************************************************/

static int
XP_MakeStringProperties(const char *dbfile)
{
    char* LibraryName;
    res_RESOURCE_TABLE* table = dsgw;
    FILE *hresfile;
    char buffer[2000];
    char *src, *dest;

    hresfile = fopen(dbfile, "w");

    if (hresfile==NULL) {
        printf("Error creating properties file %s\n",dbfile);
        return 1;
    }
 
    LibraryName = table->str;
    printf("Add Library %s\n",LibraryName);
    fprintf(hresfile, "//\n");
    fprintf(hresfile, "// #######################################\n");
    fprintf(hresfile, "// ############### %s ###############\n", LibraryName);
    fprintf(hresfile, "// ICU resource file\n");
    fprintf(hresfile, "\n");
    fprintf(hresfile, "root {\n");
    fprintf(hresfile, "\n");
    table++;
    while (table->str) {        
        /*
          Change special char to \uXXXX
        */
        src = table->str;
        dest = buffer;
        while (*src && (sizeof(buffer) > (dest-buffer))) {
            if (*src < 0x20) {
                strcpy(dest,"\\u00");
                dest += 4;
                sprintf(dest, "%02x", *src);
                dest += 1;
            }
            else if (*src == '"') { /* escape double quotes */
                strcpy(dest, "\\");
                dest++;
                *dest = *src;
            }
            else {
                *dest = *src;
            }      
            src ++;
            dest ++;
        }
        *dest = '\0';

        if (table->id > 0) {
            fprintf(hresfile, "%s%d { \"%s\" }\n", LibraryName, table->id, buffer);
        }
        table++;
    }
  
    fprintf(hresfile, "\n");
    fprintf(hresfile, "}\n");
    fclose(hresfile);
    return 0;
}




/*******************************************************************************/

int main(int argc, char *argv[])
{
    return XP_MakeStringProperties(argv[1]);
}

/*******************************************************************************/
