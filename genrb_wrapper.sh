#!/bin/sh
# BEGIN COPYRIGHT BLOCK
# Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
# Copyright (C) 2005 Red Hat, Inc.
# All rights reserved.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; version 2
# of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

# END COPYRIGHT BLOCK

# A wrapper for ICU utility genrb on Unix.
# Sets LD_LIBRARY_PATH before invoking the command

if [ $# -lt 3 ]; then
   echo Usage: $0 path_to_icu_bin_dir path_to_icu_lib_dir [ genrb parameters ]
   echo        path can be in regular PATH format with paths separated by the path delimiter
   exit 1
fi

icu_bin="$1" ; shift
icu_lib="$1" ; shift
# HP SHLIB_PATH too for old versions of HP-UX
SHLIB_PATH=$icu_lib:$SHLIB_PATH LD_LIBRARY_PATH=$icu_lib:$LD_LIBRARY_PATH PATH=$icu_bin:$PATH exec genrb "$@"
