/** --- BEGIN COPYRIGHT BLOCK ---
 * This Program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; version 2 of the License.
 * 
 * This Program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this Program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA.
 * 
 * 
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
  --- END COPYRIGHT BLOCK ---  */
/*
 * dosearch.c -- CGI search handler -- HTTP gateway
 */

#include "dsgw.h"

static void get_request(char *dn, char *ldapquery);
static void post_request();

static char *ignore_cgi_var_list[] = {
    "context", "ldq", "dn",
    "binddn", "password", "passwd",
    "ldapsizelimit", "ldaptimelimit"
};
static size_t ignore_cgi_var_list_size = sizeof(ignore_cgi_var_list)/sizeof(ignore_cgi_var_list[0]);

static int
ignore_cgi_var(const char *varname)
{
    int ii;

    if (!varname || !*varname) {
	return 1;
    }

    for (ii = 0; ii < ignore_cgi_var_list_size; ++ii) {
	if (!strcasecmp(varname, ignore_cgi_var_list[ii])) {
	    return 1;
	}
    }

    return 0;
}

int main( argc, argv, env )
    int		argc;
    char	*argv[];
#ifdef DSGW_DEBUG
    char	*env[];
#endif
{
    int		reqmethod;
    char       *dn = NULL;
    char       *ldapquery = NULL;
    int     index = 0;
    char    *varname = NULL;
    char    *val = NULL;
#ifndef __LP64__	
#ifdef HPUX
#ifndef __ia64
	/* call the static constructors in libnls */
	_main();
#endif
#endif
#endif

    reqmethod = dsgw_init( argc, argv,  DSGW_METHOD_POST | DSGW_METHOD_GET );

    ldapquery = dsgw_get_cgi_var("ldq", DSGW_CGIVAR_OPTIONAL);
    dn = dsgw_get_cgi_var("dn", DSGW_CGIVAR_OPTIONAL);
    /* 
     * If it doesn't match any of the above, or "context", then
     * tack it onto the end of ldapquery.
     */
    while ( (varname = dsgw_next_cgi_var( &index, &val )) != NULL) {
	if (ignore_cgi_var(varname)) {
	    continue;
	}
	if (ldapquery != NULL) {
	    ldapquery = dsgw_ch_realloc(ldapquery, sizeof(char *) * (strlen(ldapquery) + strlen(varname) + 2));
	    PL_strcat(ldapquery, "&");
	    PL_strcat(ldapquery, varname);
	    if (val && *val) {
		ldapquery = dsgw_ch_realloc(ldapquery, sizeof(char *) * (strlen(ldapquery) + strlen(val) + 1));
		PL_strcat(ldapquery, val);
	    }
	}
    }

    /*
     * Note: we don't call dsgw_send_header() here like we usually do because
     * on a GET we may be asked to return a MIME type other than the default
     * of text/html.  For GET requests, we send the headers inside
     * ldaputil.c:dsgw_ldapurl_search().  For POST requests, we send them
     * below in post_request().
     */

#ifdef DSGW_DEBUG
   dsgw_logstringarray( "env", env ); 
#endif

    if ( reqmethod == DSGW_METHOD_GET ) {
	get_request(dn, ldapquery);
    } else {
	post_request();
    }

    exit( 0 );
}


static void
get_request(char *dn, char *ldapquery)
{
    char  *ldapurl = NULL;

    /*
     * The following comment is kept here only as a reminder of the past.
     * It is no longer relevant. See the next comment. - RJP
     *
     * On a GET request, we do an LDAP URL search (which will just display
     * a single entry if all that is included is "host:port/DN").
     * The HTTP URL should be:
     *    .../dosearch[/host[:port]][?[dn=baseDN&][LDAPquery]]
     * This will be converted to the LDAP URL:
     *    ldap://[host[:port]]/[baseDN][?LDAPquery]
     *
     * For compatibility with prior versions, the HTTP URL may be:
     *    .../dosearch/host[:port]/[baseDN][?LDAPquery]
     * In this case, the host:port is required, since PATH_INFO can't
     * start with a '/' (web server sees that as a different program).
     * This older HTTP URL format is deprecated, because PATH_INFO is
     * not 8-bit clean on Japanese Windows NT.
     */
    
    /*
     * The only form supported now is:
     * .../dosearch?context=BLAH[&hp=host[:port]][&dn=baseDN][&ldq=LDAPquery]]  
     *   -RJP
     */
    /* richm 20080201
     * removed the hp argument - we will now just construct the ldap url
     * based on the dsgw config parameters
     */
    ldapurl = PR_smprintf("ldap%s://%s:%d/%s%s%s",
			  (gc->gc_ldapssl ? "s" : ""),
			  gc->gc_ldapserver, gc->gc_ldapport,
			  (dn ? dn : ""),
			  (ldapquery ? "?" : ""),
			  (ldapquery ? ldapquery : ""));

#ifdef DSGW_DEBUG
    dsgw_log( "get_request: processing LDAP URL \"%s\"\n", ldapurl );
#endif
    dsgw_ldapurl_search( NULL, ldapurl);

    PR_smprintf_free(ldapurl);
}


static void
post_request()
{
    char			*modestr, *searchstring, *type, *base;
    LDAP			*ld;
    LDAPFiltDesc		*lfdp;
    struct ldap_searchobj	*solistp, *sop;
    int				authmode, mode, options;

    dsgw_send_header();

    options = 0;
    modestr = dsgw_get_cgi_var( "mode", DSGW_CGIVAR_REQUIRED );
    searchstring = dsgw_get_cgi_var( "searchstring", DSGW_CGIVAR_OPTIONAL );
    dsgw_remove_leading_and_trailing_spaces( &searchstring );
#ifdef DSGW_DEBUG
    if (searchstring) {
	dsgw_log ("searchstring=\"%s\"\n", searchstring);
    } else {
	dsgw_log ("searchstring=NULL");
    }
#endif

    authmode = 0;
    if ( strcasecmp( modestr, DSGW_SRCHMODE_AUTH ) == 0 ) {
	/*
	 * treat authenticate as a variant of the smart search mode
	 */
	authmode = 1;
	mode = DSGW_SRCHMODE_SMART_ID;
	options |= DSGW_DISPLAY_OPT_AUTH;
    } else if ( strcasecmp( modestr, DSGW_SRCHMODE_SMART ) == 0 ) {
	mode = DSGW_SRCHMODE_SMART_ID;
    } else if ( strcasecmp( modestr, DSGW_SRCHMODE_COMPLEX ) == 0 ) {
	mode = DSGW_SRCHMODE_COMPLEX_ID;
    } else if (  strcasecmp( modestr, DSGW_SRCHMODE_PATTERN ) == 0 ) {
	mode = DSGW_SRCHMODE_PATTERN_ID;
    } else {
	dsgw_error( DSGW_ERR_SEARCHMODE, modestr, 0, 0, NULL );
	mode = 0;
    }

    if ( mode != DSGW_SRCHMODE_PATTERN_ID
	    && ( searchstring == NULL || *searchstring == '\0' )) {
	dsgw_error( DSGW_ERR_NOSEARCHSTRING, NULL, DSGW_ERROPT_EXIT, 0, NULL );
    }

    if (( type = dsgw_get_cgi_var( "type", authmode ? DSGW_CGIVAR_OPTIONAL :
	    DSGW_CGIVAR_REQUIRED )) == NULL ) {
	type = DSGW_SRCHTYPE_AUTH;
    }

    if (( base = dsgw_get_cgi_var( "base", DSGW_CGIVAR_OPTIONAL )) == NULL ) {
	base = gc->gc_ldapsearchbase;
    }

    /* check for options (carried in boolean CGI variables) */
    if ( dsgw_get_boolean_var( "listifone", DSGW_CGIVAR_OPTIONAL, 0 )) {
	options |= DSGW_DISPLAY_OPT_LIST_IF_ONE;
    }

    if ( dsgw_get_boolean_var( "editable", DSGW_CGIVAR_OPTIONAL, 0 )) {
	options |= DSGW_DISPLAY_OPT_EDITABLE;
    }

    if ( dsgw_get_boolean_var( "link2edit", DSGW_CGIVAR_OPTIONAL, 0 )) {
	options |= DSGW_DISPLAY_OPT_LINK2EDIT;
    }

    if ( dsgw_get_boolean_var( "dnlist_js", DSGW_CGIVAR_OPTIONAL, 0 )) {
	options |= DSGW_DISPLAY_OPT_DNLIST_JS;
    }

    (void) dsgw_init_ldap( &ld, &lfdp, ( authmode == 1 ) ? 1 : 0, 0);

    if ( mode != DSGW_SRCHMODE_PATTERN_ID ) {
	dsgw_init_searchprefs( &solistp );

	if (( sop = dsgw_type2searchobj( solistp, type )) == NULL ) {
	    ldap_unbind_ext( ld, NULL, NULL );
	    dsgw_error( DSGW_ERR_UNKSRCHTYPE, type, DSGW_ERROPT_EXIT, 0, NULL );
	}
    }

    switch( mode ) {
    case DSGW_SRCHMODE_SMART_ID:
	/*
	 * smart search mode -- try to do the right kind of search for the
	 * client based on what the user entered in the search box
	 */
	dsgw_smart_search( ld, sop, lfdp, base, searchstring, options );
	break;

    case DSGW_SRCHMODE_COMPLEX_ID: {
	/*
	 * complex search mode -- construct a specific filter based on
	 * user's form selections
	 */
	int			scope;
	char			*attrlabel, *matchprompt;
	struct ldap_searchattr	*sap;
	struct ldap_searchmatch	*smp;

	attrlabel = dsgw_get_cgi_var( "attr", DSGW_CGIVAR_REQUIRED );
	if (( sap = dsgw_label2searchattr( sop, attrlabel )) == NULL ) {
	    ldap_unbind_ext( ld, NULL, NULL );
	    dsgw_error( DSGW_ERR_UNKATTRLABEL, attrlabel, DSGW_ERROPT_EXIT,
		    0, NULL );
	}
	
	matchprompt = dsgw_get_cgi_var( "match", DSGW_CGIVAR_REQUIRED );
	if (( smp = dsgw_prompt2searchmatch( sop, matchprompt )) == NULL ) {
	    ldap_unbind_ext( ld, NULL, NULL );
	    dsgw_error( DSGW_ERR_UNKMATCHPROMPT, matchprompt,
		    DSGW_ERROPT_EXIT, 0, NULL );
	}

	scope = dsgw_get_int_var( "scope", DSGW_CGIVAR_OPTIONAL,
		sop->so_defaultscope );
	dsgw_pattern_search( ld, sop->so_objtypeprompt,
			     sap->sa_attrlabel, smp->sm_matchprompt, searchstring,
			     smp->sm_filter, sop->so_filterprefix, NULL, sap->sa_attr,
			     base, scope, searchstring, options );
    }
	break;

    case DSGW_SRCHMODE_PATTERN_ID: {
	/*
	 * pattern-based search mode (no searchprefs or filter file used)
	 */
	char	*attr, *pattern, *prefix, *suffix, *searchdesc;
	int	scope;

	attr = dsgw_get_cgi_var( "attr", DSGW_CGIVAR_REQUIRED );
	pattern = dsgw_get_cgi_var( "filterpattern", DSGW_CGIVAR_REQUIRED );
	prefix = dsgw_get_cgi_var( "filterprefix", DSGW_CGIVAR_OPTIONAL );
	suffix = dsgw_get_cgi_var( "filtersuffix", DSGW_CGIVAR_OPTIONAL );
	scope = dsgw_get_int_var( "scope", DSGW_CGIVAR_OPTIONAL,
		LDAP_SCOPE_SUBTREE );
	options |= DSGW_DISPLAY_OPT_CUSTOM_SEARCHDESC;
	searchdesc = dsgw_get_cgi_var( "searchdesc", DSGW_CGIVAR_OPTIONAL );
	dsgw_pattern_search( ld, type, searchdesc, NULL, NULL,
		pattern, prefix, suffix, attr,
		base, scope, searchstring, options );
	}
	break;
    }

    ldap_unbind_ext( ld, NULL, NULL );
}

/*
  emacs settings
  Local Variables:
  indent-tabs-mode: t
  tab-width: 8
  End:
*/
