/** --- BEGIN COPYRIGHT BLOCK ---
 * This Program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; version 2 of the License.
 * 
 * This Program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this Program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA.
 * 
 * 
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
  --- END COPYRIGHT BLOCK ---  */

#include <stdio.h>
#include "dsgw.h"

#include <ssl.h>

main( int argc, char **argv)
{
    char *p;
    char dn[ 512 ];
    char pw[ 512 ];
    char lifesec[ 512 ];
    int rc;
    int c;
    extern char *optarg;
    time_t lifetime;
    
    printf( "Add an entry to the cookie database\n" );

	dn[0] = pw[0] = lifesec[0] = 0;
    if ( argc > 1 ) {
	while (( c = getopt( argc, argv, "d:l:p:" )) != EOF ) {
	    switch ( c ) {
	    case 'd':
		PL_strncpyz( dn, optarg, sizeof(dn) );
		break;
	    case 'l':
		PL_strncpyz( lifesec, optarg, sizeof(lifesec) );
		break;
	    case 'p':
		PL_strncpyz( pw, optarg, sizeof(pw) );
		break;
	    }
	}
    }

    if ( strlen( dn ) == 0 || strlen( pw ) == 0 || strlen( lifesec ) == 0 ) {
	printf( "dn: " );
	fgets( dn, sizeof(dn), stdin );
	if (p = strchr(dn, '\n')) {
		*p = 0;
	}
	printf( "passwd: " );
	fgets( pw, sizeof(pw), stdin );
	if (p = strchr(pw, '\n')) {
		*p = 0;
	}
	printf( "expires in how many seconds? " );
	fgets( lifesec, sizeof(lifesec), stdin );
	if (p = strchr(lifesec, '\n')) {
		*p = 0;
	}
    }

    lifetime = atol( lifesec );
    p = dsgw_mkcookie( dn, pw, lifetime, &rc );
    if ( p == NULL ) {
	fprintf( stderr, "Error storing cookie: error %d\n", rc );
    } else {
	printf( "success, cookie is %s\n", p );
    }
}
