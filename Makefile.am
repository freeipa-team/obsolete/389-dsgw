# BEGIN COPYRIGHT BLOCK
# Copyright (C) 2006 Red Hat, Inc.
# All rights reserved.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; version 2
# of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# END COPYRIGHT BLOCK

# look for included m4 files in the ./m4/ directory
ACLOCAL_AMFLAGS = -I m4

instconfigdir = @instconfigdir@
cgibindir = $(libdir)@cgibindir@
htmldir = $(datadir)@htmldir@
pbhtmldir = $(datadir)@pbhtmldir@
orghtmldir = $(datadir)@orghtmldir@
admservdata = $(datadir)@admservdata@
admservhtml = $(datadir)@admservhtml@
# config is a bit of a misnomer - these are really configurable templates
configdir = $(datadir)@configdir@
pbconfigdir = $(datadir)@pbconfigdir@
propertydir=$(datadir)@propertydir@
# the context dir is where the application specific config files go
contextdir = $(instconfigdir)@contextdir@
securitydir=$(instconfigdir)@securitydir@
# relative to $localstatedir
cookiedir=$(localstatedir)@cookiedir@
manualdir = $(datadir)@manualdir@
manualsubdir = @manualsubdir@
# this is the directory where the manuals will actually be installed
maninstdir = $(manualdir)/en/$(manualsubdir)
perldir = $(libdir)@perldir@

# for a bundle, all of the components will be under libdir (e.g. prefix/lib)
if BUNDLE
perlpath=$(perldir) $(libdir)/perl/arch $(libdir)/perl
else
perlpath=$(perldir)
endif

DSGW_VER_STR := "Directory-Server-Gateway/$(PACKAGE_VERSION)"

AM_CPPFLAGS = -DDSGW_VER_STR=\"$(DSGW_VER_STR)\" $(DEBUG_DEFINES) @adminutil_inc@ @icu_inc@ @openldap_inc@ @ldapsdk_inc@ @nss_inc@ @nspr_inc@ \
	-I$(srcdir)/include -I$(srcdir)/include/base
if WINNT
AM_CPPFLAGS += -DXP_WINNT
else
AM_CPPFLAGS += -DXP_UNIX
endif

AM_CPPFLAGS +=-DPROPERTYDIR=\"$(propertydir)\" \
	-DHTMLDIR=\"$(htmldir)\" -DCOOKIEDIR=\"$(cookiedir)\" \
	-DCONFIGDIR=\"$(configdir)\" -DSECURITYDIR=\"$(securitydir)\" \
	-DCGIBINDIR=\"$(cgibindir)\" -DCONTEXTDIR=\"$(contextdir)\" \
	-DINSTCONFIGDIR=\"$(instconfigdir)\" -DMANUALDIR=\"$(manualdir)\" \
	-DCGIURIBASE=\"$(cgiuri)\" -DMANUALSUBDIR=\"$(manualsubdir)\"

if OPENLDAP
LDAPSDK_LINK = @openldap_lib@ -lldap@ol_libver@ @ldap_lib_ldif@ -llber@ol_libver@
else
LDAPSDK_LINK = @ldapsdk_lib@ -lssldap60 -lprldap60 -lldap60 -lldif60
endif

DSGW_LIBS = @adminutil_lib@ -ladmsslutil@adminutil_ver@ -ladminutil@adminutil_ver@ \
	@icu_lib@ -licui18n -licuuc -licudata \
	$(LDAPSDK_LINK) \
	@sasl_lib@ -lsasl2 \
	@nss_lib@ -lsmime3 -lssl3 -lnss3 -lsoftokn3 \
	@nspr_lib@ -lplds4 -lplc4 -lnspr4

noinst_PROGRAMS = propmaker

# We need to make sure that libpthread is linked before libc on HP-UX.
if HPUX
AM_LDFLAGS = -lpthread
endif

# this is just a dummy to make automake define
# the CXXLINK macro
if CXXLINK_REQUIRED
noinst_PROGRAMS += dummy
dummy_SOURCES = dummy.cpp
dummy_LINK = $(CXXLINK)
endif

if CXXLINK_REQUIRED
MYLINK = $(CXXLINK)
else
MYLINK = $(LINK)
endif

# these are programs which we do not want to link with nss
NEED_SECGLUE = unauth search csearch newentry tutor lang
# these are programs which are not used at runtime but may be useful
# to administer the cookie repository
#CKUTILPROGS= ckput ckget ckdump ckpurge ckdel

cgibin_PROGRAMS = auth doauth edit domodify dnedit dosearch $(NEED_SECGLUE) $(CKUTILPROGS)

# for c++ linkage on those platforms that require it
auth_LINK = $(MYLINK)
doauth_LINK = $(MYLINK)
edit_LINK = $(MYLINK)
domodify_LINK = $(MYLINK)
dnedit_LINK = $(MYLINK)
dosearch_LINK = $(MYLINK)
unauth_LINK = $(MYLINK)
search_LINK = $(MYLINK)
csearch_LINK = $(MYLINK)
newentry_LINK = $(MYLINK)
tutor_LINK = $(MYLINK)
lang_LINK = $(MYLINK)

cgibin_SCRIPTS = orgbin/org orgbin/myorg
if DEBUG
DBGSCRIPTS = $(addsuffix .sh,$(cgibin_PROGRAMS))
cgibin_SCRIPTS += $(DBGSCRIPTS)
endif

# mozldap provides utf8 string support and templating/formatting
# support which openldap does not - in addition, the sort APIs
# from openldap are deprecated - so we use these files from
# mozldap
if OPENLDAP
MOZLDAP_SOURCES = srchpref.c utf8.c sort.c dsparse.c tmplout.c disptmpl.c
endif

COMMON_SOURCES = htmlout.c htmlparse.c error.c cgiutil.c dsgwutil.c ldaputil.c \
	entrydisplay.c config.c	cookie.c emitauth.c emitf.c collate.c vcard.c \
	Versiongw.c utf8compare.c dsgwgetlang.c $(MOZLDAP_SOURCES)

unauth_SOURCES = unauth.c $(COMMON_SOURCES) secglue.c
unauth_LDADD = $(DSGW_LIBS)
search_SOURCES = search.c $(COMMON_SOURCES) secglue.c
search_LDADD = $(DSGW_LIBS)
csearch_SOURCES = csearch.c $(COMMON_SOURCES) secglue.c
csearch_LDADD = $(DSGW_LIBS)
newentry_SOURCES = newentry.c $(COMMON_SOURCES) secglue.c
newentry_LDADD = $(DSGW_LIBS)
tutor_SOURCES = tutor.c $(COMMON_SOURCES) secglue.c
tutor_LDADD = $(DSGW_LIBS)
lang_SOURCES = lang.c $(COMMON_SOURCES) secglue.c
lang_LDADD = $(DSGW_LIBS)

auth_SOURCES = auth.c $(COMMON_SOURCES)
auth_LDADD = $(DSGW_LIBS)
doauth_SOURCES = doauth.c $(COMMON_SOURCES)
doauth_LDADD = $(DSGW_LIBS)
edit_SOURCES = edit.c $(COMMON_SOURCES)
edit_LDADD = $(DSGW_LIBS)
domodify_SOURCES = domodify.c $(COMMON_SOURCES)
domodify_LDADD = $(DSGW_LIBS)
dnedit_SOURCES = dnedit.c $(COMMON_SOURCES)
dnedit_LDADD = $(DSGW_LIBS)
dosearch_SOURCES = dosearch.c $(COMMON_SOURCES)
dosearch_LDADD = $(DSGW_LIBS)

auth_DEPENDENCIES = resource_bundles

# ckput_SOURCES = ckput.c $(COMMON_SOURCES)
# ckget_SOURCES = ckget.c $(COMMON_SOURCES)
# ckdump_SOURCES = ckdump.c $(COMMON_SOURCES)
# ckpurge_SOURCES = ckpurge.c $(COMMON_SOURCES)
# ckdel_SOURCES = ckdel.c $(COMMON_SOURCES)

############## Data Files ################
dist_html_DATA = \
	html/aim-online.gif     html/eduser.html      html/newentrytitle.html \
	html/alert.gif          html/emptyFrame.html  html/organization.gif \
	html/alert.html         html/exit1.gif        html/orgicon.gif \
	html/auth.html          html/forward1.gif     html/orgunit.gif \
	html/authroot.html      html/greeting.html    html/person.gif \
	html/authtitle.html     html/group.gif        html/right_bottom.gif \
	html/back1.gif          html/index1.gif       html/right_off.gif \
	html/clear.gif          html/index.html       html/right_on.gif \
	html/confirm.gif        html/left_bottom.gif  html/rolodex.gif \
	html/confirm.html       html/left_off.gif     html/searchtitle.html \
	html/content1.gif       html/left_on.gif      html/style.css \
	html/country.gif        html/maintitle.html   html/transparent.gif \
	html/csearchtitle.html  html/dc.gif           html/message.gif

dist_pbhtml_DATA = \
	pbhtml/16-conference.gif  pbhtml/department.gif        pbhtml/person.gif \
	pbhtml/16-person.gif      pbhtml/emptyFrame.html       pbhtml/phone.html \
	pbhtml/32-alert.gif       pbhtml/get_cert.gif          pbhtml/phone.js \
	pbhtml/32-conference.gif  pbhtml/get_cert_sm.gif       pbhtml/pixel.gif \
	pbhtml/32-message.gif     pbhtml/index.html            pbhtml/report.html \
	pbhtml/32-office.gif      pbhtml/intro.html            pbhtml/rolodex.gif \
	pbhtml/32-person.gif      pbhtml/pbrd.jpg              pbhtml/style.css \
	pbhtml/aim-online.gif     pbhtml/modify.html           pbhtml/tiny_cert.gif \
	pbhtml/alert.html         pbhtml/nonemp.html           pbhtml/tiny_vcard.gif \
	pbhtml/carded.html        pbhtml/nullStringError.html  pbhtml/vendor.gif \
	pbhtml/clear.gif          pbhtml/office.gif            pbhtml/view_vcard.gif \
	pbhtml/conference.gif     pbhtml/orgicon.gif           pbhtml/view_vcard_sm.gif \
	pbhtml/confirm.html

dist_orghtml_DATA = \
	orghtml/aim-online.gif		orghtml/arrow.gif		orghtml/botframe.html \
	orghtml/branch-cc1.gif		orghtml/index.html		orghtml/ldap-person.gif \
	orghtml/mag.gif			orghtml/mail.gif		orghtml/new-branch-blank.gif \
	orghtml/new-branch-first.gif	orghtml/new-branch-straight.gif	orghtml/orgicon.gif \
	orghtml/styles.css		orghtml/topframe.html orghtml/starthelp.gif orghtml/orgchart.tmpl


dist_config_DATA = \
	config/authPassword.html          config/dsgw-l10n.conf \
	config/authSearch.html            config/dsgwsearchprefs.conf \
	config/csearchAttr.html           config/dsgw.tmpl \
	config/csearchBase.html           config/edit-passwd.html \
	config/csearch.html               config/list-Anything.html \
	config/csearchMatch.html          config/list-Auth.html \
	config/csearchString.html         config/list-Domaincomponent.html \
	config/csearchType.html           config/list-fa-Groups.html \
	config/display-country.html       config/list-fa-People.html \
	config/display-dc.html            config/list-Groups.html \
	config/display-dnedit.html        config/list-NT-Groups.html \
	config/display-dneditpeople.html  config/list-NT-People.html \
	config/display-group.html         config/list-Organizations.html \
	config/display-groupun.html       config/list-Org-Units.html \
	config/display-ntgroup.html       config/list-People.html \
	config/display-ntperson.html      config/list-urlsearch.html \
	config/display-org.html            \
	config/display-orgperson.html     config/newentry.html \
	config/display-orgunit.html       config/newentryName.html \
	config/display-person.html        config/newentryType.html \
	config/orgchart.tmpl              config/search.html \
	config/dsgwfilter.conf            config/searchString.html \
	config/en/dsgwcollate.conf        config/en/dsgw-l10n.conf

dist_pbconfig_DATA = \
	pbconfig/authPassword.html       pbconfig/dsgwsearchprefs.conf \
	pbconfig/authSearch.html         pbconfig/edit-passwd.html \
	pbconfig/display-orgperson.html  pbconfig/list-Auth.html \
	pbconfig/display-orgunit.html    pbconfig/list-People.html \
	pbconfig/display-room.html       \
	pbconfig/dsgwfilter.conf         pbconfig/pb.tmpl

dist_maninst_DATA = \
	html/manual/a.gif html/manual/add.htm html/manual/attribua.gif html/manual/attribut.htm html/manual/auth.htm \
	html/manual/contents.html html/manual/intro.htm html/manual/mod.htm html/manual/n.gif \
	html/manual/objclass.htm html/manual/search.htm html/manual/t.gif html/manual/y.gif html/manual/index.map \
	html/info/infonav.html

nodist_context_DATA = dsgw-httpd.conf
nodist_sbin_SCRIPTS = setup-ds-dsgw

# add more here for localized bundles
nodist_property_DATA = root.res en.res en_US.res

MOSTLYCLEANFILES = dsgw.conf root.res dsgw.properties setup dsgw-httpd.conf en.res en_US.res

# Resource Bundle Compiler 
if WINNT
ICU_GENRB = @icu_bin@/genrb.exe
else
ICU_GENRB = sh -x $(srcdir)/genrb_wrapper.sh "@icu_bin@" "@icu_libdir@"
endif

# The root resource bundle is based on English (en) locale;
# This bundle must be always distributed and there is no need to have
# *_en.properties resource bundle source files.
RESOURCE_BUNDLES_ROOT = root.res

# French resource bundles (for the French localization in the future)
RESOURCE_BUNDLES_FR = fr.res

# German resource bundles (for the German localization in the future)
RESOURCE_BUNDLES_DE = de.res

# By default create only the default root bundle (english).
# Other locales should be created during the localization process.
# RESOURCE_BUNDLES_ROOT must be always compiled.
resource_bundles: $(RESOURCE_BUNDLES_ROOT)

.PHONY: resource_bundles

dsgw.properties: ./propmaker dbtdsgw.h
	./propmaker $@

root.res : dsgw.properties
	$(ICU_GENRB) -s. -d. --encoding 8859-1 $+

fr.res : fr.properties
	$(ICU_GENRB) -s. -d. --encoding 8859-2 $+

de.res : de.properties
	$(ICU_GENRB) -s. -d. --encoding 8859-2 $+

en.res en_US.res : root.res
	cp -p $< $@

# these are for the config files and scripts that we need to generate and replace
# the paths and other tokens with the real values set during configure/make
# note that we cannot just use AC_OUTPUT to do this for us, since it will do things like this:
# ADMConfigDir = ${prefix}/etc/packagename
# i.e. it literally copies in '${prefix}' rather than expanding it out - we want this instead:
# ADMConfigDir = /etc/packagename
fixupcmd = sed \
        -e 's,@bindir\@,$(bindir),g' \
        -e 's,@sbindir\@,$(sbindir),g' \
        -e 's,@localstatedir\@,$(localstatedir),g' \
        -e 's,@cgibindir\@,$(cgibindir),g' \
        -e 's,@cgiuri\@,$(cgiuri),g' \
	-e 's,@admservdata\@,$(admservdata),g' \
	-e 's,@admservhtml\@,$(admservhtml),g' \
	-e 's,@orguri\@,@orguri@,g' \
	-e 's,@dsgwuri\@,@dsgwuri@,g' \
	-e 's,@pburi\@,@pburi@,g' \
        -e 's,@cmdbindir\@,$(cmdbindir),g' \
        -e 's,@propertydir\@,$(propertydir),g' \
        -e 's,@htmldir\@,$(htmldir),g' \
        -e 's,@pbhtmldir\@,$(pbhtmldir),g' \
	-e 's,@orghtmldir\@,$(orghtmldir),g' \
        -e 's,@configdir\@,$(configdir),g' \
        -e 's,@pbconfigdir\@,$(pbconfigdir),g' \
        -e 's,@contextdir\@,$(contextdir),g' \
        -e 's,@securitydir\@,$(securitydir),g' \
        -e 's,@instconfigdir\@,$(instconfigdir),g' \
        -e 's,@cookiedir\@,$(cookiedir),g' \
	-e 's,@perlpath\@,$(perlpath),g' \
	-e 's,@perlexec\@,@perlexec@,g' \
	-e 's,@manualdir\@,$(manualdir),g' \
       -e 's,@BUILD_NUM\@,$(BUILDNUM),g' \
        -e 's,@NQBUILD_NUM\@,$(NQBUILDNUM),g' \
        -e 's,@package_name\@,$(PACKAGE_NAME),g' \
        -e 's,@PACKAGE_BASE_NAME\@,$(PACKAGE_BASE_NAME),g' \
        -e 's,@PACKAGE_VERSION\@,$(PACKAGE_VERSION),g' \
        -e 's,@PACKAGE_BASE_VERSION\@,$(PACKAGE_BASE_VERSION),g' \
        -e 's,@brand\@,$(brand),g' \
        -e 's,@capbrand\@,$(capbrand),g' \
        -e 's,@vendor\@,$(vendor),g' \
		-e "s,@progname\@,$$progname,g"

if DEBUG
$(DBGSCRIPTS) : cgidbgwrapper.sh.in
	if [ ! -d $(dir $@) ] ; then mkdir -p $(dir $@) ; fi
	progname=`basename $@ .sh` ; $(fixupcmd) $< > $@
endif

% : %.in
	if [ ! -d $(dir $@) ] ; then mkdir -p $(dir $@) ; fi
	$(fixupcmd) $< > $@
