/** --- BEGIN COPYRIGHT BLOCK ---
 * This Program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; version 2 of the License.
 * 
 * This Program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this Program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA.
 * 
 * 
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
  --- END COPYRIGHT BLOCK ---  */

#include <stdio.h>
#include "dsgw.h"

#include <ssl.h>
#include <sec.h>

main()
{
#ifdef notdef /* this was some testing code... */
{
    char *ck, *r, *d, *p;
    int rc;

    ck = dsgw_get_auth_cookie();
    rc = dsgw_parse_cookie( ck, &r, &d );
    if ( rc == 0 ) {
	(void) dsgw_ckdn2passwd( r, d, &p );
	printf( "Got pw of <%s>\n", ( p == NULL ) ? "NULL" : p );
    }
}
#endif /* notdef */
    printf( "Dump the cookie database\n" );

    dsgw_traverse_db();
}
