/** --- BEGIN COPYRIGHT BLOCK ---
 * This Program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; version 2 of the License.
 * 
 * This Program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this Program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA.
 * 
 * 
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
  --- END COPYRIGHT BLOCK ---  */
/* 
 * Convert a document from ../html, or redirect the server to it.
 */

#include "dsgw.h"
#include "dbtdsgw.h"

#ifdef XP_WIN
#define PATH_SLASH "\\"
#else
#define PATH_SLASH "/"
#endif

static int
doc_is_UTF_8 (const char* docname)
{
    static const char* suffixes [] = {".html", ".htm", NULL};
    const size_t doclen = strlen (docname);
    const char** suf = suffixes;
    for (suf = suffixes; *suf; ++suf) {
	const size_t suflen = strlen (*suf);
	if (doclen >= suflen && !strcasecmp (*suf, docname + doclen - suflen)) {
	    return 1;
	}
    }
    return 0;
}

static const char*
skip_prefix (const char* prefix, const char* s)
{
    const size_t prelen = strlen (prefix);
    if (!strncmp (prefix, s, prelen)) return s + prelen;
    return s;
}

static int
doc_convert( FILE** fpp, char* stop_at_directive, int erropts )
{
    char	**argv, line[ BIG_LINE ];
    int		argc;
 
    while ( dsgw_next_html_line( *fpp, line ))  {
	if ( dsgw_parse_line( line, &argc, &argv, 0, dsgw_simple_cond_is_true,
		NULL )) {
	    if ( stop_at_directive != NULL &&
		    dsgw_directive_is( line, stop_at_directive )) {
		return( 0 );

	    } else if ( dsgw_directive_is( line, DRCT_HEAD )) {
		dsgw_head_begin();
		dsgw_emits ("\n");

	    } else if ( dsgw_directive_is( line, DRCT_DS_POSTEDVALUE )) {
		dsgw_emit_cgi_var (argc, argv);

	    } else if ( dsgw_directive_is( line, DRCT_DS_CLOSEBUTTON )) {
		dsgw_emit_button (argc, argv, "onClick=\"top.close()\"");

	    } else if ( dsgw_directive_is( line, "DS_CONFIRM_SCRIPT" )) {
		dsgw_emit_confirm_script();
	    } else if ( dsgw_directive_is( line, "DS_CONFIRM_BUTTON_OK" )) {
		dsgw_emitf ("<INPUT TYPE=BUTTON VALUE=\"%s\" onClick=\"parent.OK()\">\n",
			    XP_GetClientStr(DBT_ok_2));

	    } else if ( dsgw_directive_is( line, "DS_CONFIRM_BUTTON_CANCEL" )) {
		dsgw_emitf ("<INPUT TYPE=BUTTON VALUE=\"%s\" onClick=\"parent.Cancel()\">\n",
			    XP_GetClientStr(DBT_cancel_2));

	    } else {
		dsgw_emits (line);
	    }
	}
    }
    fclose( *fpp );
    *fpp = NULL;
    return( 0 );
}

int
main( int argc, char *argv[]
#ifdef DSGW_DEBUG
     , char *env[]
#endif
     )
{
    /*static char* docdir = ".." PATH_SLASH "html" PATH_SLASH;*/
    static char* docdir = NULL;
    static char* helpdir = NULL;
    char* docname = NULL;
    char* tfname = NULL;
    int result = 0;
    int  manual_file = 0; /* Flag: is the file a documentation file? */
    
    (void)dsgw_init( argc, argv, DSGW_METHOD_GET | DSGW_METHOD_POST );

    dsgw_last_op_info = dsgw_get_cgi_var("info", DSGW_CGIVAR_OPTIONAL);
    docname = dsgw_get_cgi_var("file", DSGW_CGIVAR_OPTIONAL);
    if (docname) {
	/*If we're handling a help page, forgo the filename check*/
	if ( strlen( docname ) > DSGW_MANUALSHORTCUT_LEN && 
	     strncmp( docname, DSGW_MANUALSHORTCUT, 
		      DSGW_MANUALSHORTCUT_LEN ) == 0 ) {
	    manual_file = 1;
	}
	/*
	 * Make sure the person isn't trying to get 
	 * some file not in the gateway.
	 */
	if (manual_file == 0 && !dsgw_valid_docname(docname)) {
	    dsgw_error( DSGW_ERR_BADFILEPATH, docname, 
			DSGW_ERROPT_EXIT, 0, NULL );
	}
    }

    docdir = dsgw_get_docdir();
    
    /*If there is no docname, default to index.html*/
    if (docname == NULL) {
      docname = dsgw_ch_strdup("index.html");
    }

    /* I think this is a no op - dsgw_valid_docname will reject "/" */
    if (!strcmp (docname, "/")) {
	printf( "Location: %s?context=%s\n\n", 
		dsgw_getvp( DSGW_CGINUM_SEARCH ), context );
	return( result );
    } /* I think this is a no op? else {
	char* p;
	if (*docname == '/') ++docname;
	docname = dsgw_ch_strdup( docname );
	if (( p = strrchr( docname, '&' )) != NULL ) {
	    *p++ = '\0';
	    if ( strncasecmp( p, "info=", 5 ) == 0 ) {
		dsgw_last_op_info = dsgw_ch_strdup( p + 5 );
		dsgw_form_unescape( dsgw_last_op_info );
	    }
	}
    } */
    
    if (manual_file) {
        /* check filename */
        char *mandocname = dsgw_ch_strdup(docname + DSGW_MANUALSHORTCUT_LEN);
        if (*mandocname == '/') {
            if (!dsgw_valid_docname(mandocname+1)) {
                dsgw_error( DSGW_ERR_BADFILEPATH, mandocname, 
                            DSGW_ERROPT_EXIT, 0, NULL );
            }
        } else {
            if (!dsgw_valid_docname(mandocname)) {
                dsgw_error( DSGW_ERR_BADFILEPATH, mandocname, 
                            DSGW_ERROPT_EXIT, 0, NULL );
            }
        }

	helpdir = dsgw_file2path ( DSGW_MANROOT, DSGW_MANSUBDIR );
	tfname = PR_smprintf("%s/%s", helpdir, mandocname);
	free( helpdir );
        free( mandocname );

    } else {
	tfname = dsgw_file2path (docdir, docname);
    }

    if ( ! doc_is_UTF_8 (tfname)) { /* Redirect the Web server: */
	printf ("Location: %s%s%s\n\n", 
		dsgw_server_url(), gc->gc_gwnametrans, skip_prefix (docdir, tfname));
	/* It's tempting to also redirect if is_UTF_8(gc->gc_charset).
	   But it would be wrong: the Web server would transmit an
	   HTTP Content-type with no charset parameter.  The header
	   must include ";charset=UTF-8".  So we transmit it:
	*/
    } else { /* Transmit the document: */
	const int erropts = DSGW_ERROPT_EXIT;
	auto FILE* docfile;

	dsgw_send_header();
#ifdef DSGW_DEBUG
	dsgw_logstringarray( "env", env ); 
#endif
	if ((docfile = fopen(tfname, "r")) == NULL) {
	    dsgw_error( DSGW_ERR_OPENHTMLFILE, tfname, erropts, 0, NULL );
	    return( -1 );
	}
	result = doc_convert( &docfile, NULL, erropts );
    }
/*
 * XXXmcs: the following free() causes a crash on NT... so don't do it!
 */
    free(tfname);

    return result;
}

/*
  emacs settings
  Local Variables:
  indent-tabs-mode: t
  tab-width: 8
  End:
*/
